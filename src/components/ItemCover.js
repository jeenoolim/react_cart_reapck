import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './itemCover.css';


export default class ItemCover extends Component {
    static propTypes = {
        data: PropTypes.object,
    };
    

    
    render() {
        let { data } = this.props;
        return (
            <div className={ styles.container }>
                <div className={ `${styles.cover} ${styles.soldout}` }
                    style={{ display: data.soldout ? 'block' : 'none' }} />
                <div className={ styles.cover }
                    style={{ display: data.closed ? 'block' : 'none' }}>판매종료</div>
            </div>
        );
    }
}
