import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { numberTween } from '../utils/UtilMotion';
import { formatLocal, formatCreditcard, formatNumber, words } from '../utils/UtilString';
import PropTypes from 'prop-types';
import equal from 'deep-equal';
import ActionCreators from '../actions/ActionCreators';
import CashbagStore from '../stores/CashbagStore';
import ItemStore from '../stores/ItemStore';
import Alert from '../ui/Alert';
import Config from '../Config';
import ReactDOM from 'react-dom';
import styles from './cashbagViewer.css';
import Button from '../ui/Button';

const KEY_BACKSPACE = 8;
const MIN_CARDNUMBER_LENGTH = 15;
const MAX_CARDNUMBER_LENGTH = 16;
const ALLERT_INPUT_ERROR = '카드번호를 확인 해 주세요.';
const ALLERT_INVALID_PASSWORD = "비밀번호를 입력 해 주세요.";
const ALLERT_INVALID_POINT = '사용할 포인트를 입력 해 주세요.';

class CashbagViewer extends Component {

    static propTypes = {
        active: PropTypes.bool,
    }

    static getStores(){
        return [CashbagStore, ItemStore];
    }

    static calculateState(prevState){
        return {
            registed: CashbagStore.registed,
            number: CashbagStore.number,
            password: CashbagStore.password,
            requestId: CashbagStore.requestId,
            error: CashbagStore.error,
            total: CashbagStore.total,
            spent: CashbagStore.spent,
            totalCost: ItemStore.totalCost,
        }
    }

    /**
     * 생성자
     * @param  {[object]} props
     * @return {[void]}
     */
    constructor(props) {
        super(props);
        this.cashbag = { value: 0 };
        this.state={
            mode: 'text',
            cashbag: 0,
            point: 0
        }

        this.result = null;''
        this.resultHeight = 0;
        this.loading = null;
        this.loadingY = 0;
        this.loadingHeight = 0;
        this.submit = null;
        this.submitHeight = 0;
        this.error = null;
        this.errorHeight = 0;
        this.preloader = null;

        this.initialize = this.initialize.bind(this);
        this.onFullUse = this.onFullUse.bind(this);
        this.onChangeNumber = this.onChangeNumber.bind(this);

        this.onFocusPoint = this.onFocusPoint.bind(this);
        this.onBlurPoint = this.onBlurPoint.bind(this);
        this.onChangePoint = this.onChangePoint.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);

        this.onSubmit = this.onSubmit.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onConfirm = this.onConfirm.bind(this);
    }



    /**
     * 초기 레이아웃 높이 저장및 설정
     * @return {[void]}
     */
    initialize(){
        this.loadingHeight = this.loading.offsetHeight;
        this.resultHeight = this.result.offsetHeight;
        this.submitHeight = this.submit.offsetHeight;
        this.errorHeight = this.error.offsetHeight;
        TweenMax.set([this.loading, this.result, this.error], { height: 0 });
        TweenMax.set(this.layout, { perspective: 600 });
        TweenMax.set(this.back, { rotationY: 180 });
        TweenMax.set([this.front, this.back], { backfaceVisibility: 'hidden' });
    }

    /**
     * 컴포넌트 마운트
     * @return {[void]}
     * 저장된 고객 오케이캐쉬백카드번호 요청
     */
    componentDidMount() {
        ActionCreators.requestCashbagCardNumber();
        setTimeout(this.initialize, 0);
    }

    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    shouldComponentUpdate(nextProps, nextState) {
        const propsEqual = equal(this.props, nextProps),
            stateEqual = equal(this.state, nextState);
        return !propsEqual || !stateEqual;
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.active && this.state.point !== this.state.spent){
            this.setState({ point: this.state.spent });
        }
    }

    /**
     * 컴포넌트 업데이트
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     * 오케이캐쉬백 잔여 포인트 애니메이션
     * 요청아이디가 다르면 요청 결과 애니메이션
     */
    componentWillUpdate(nextProps, nextState) {
        // this.setState({ point: nextState.spent});
        numberTween(this.cashbag, (nextState.total - formatNumber(nextState.point)), (tween) => {
            this.setState({ cashbag: tween.value.toFixed(0) });
        }, 'value', 0.1);

        if(this.state.requestId === nextState.requestId){
            return false;
        }
        if(!nextState.error){
            this.enter(true);
            this.show('result', true);
        }else{
            this.show('error', true);
        }

        this.show('loading', false);
        this.show('submit', true);
    }

    /**
     * 오케이 캐쉬백 카드번호 입력시
     * @param  {[object]} e]
     * @return {[void]}
     * 카드형태의 문자열로 변환
     */
    onChangeNumber(e){
        let value = words(e.target.value).substring(0, MAX_CARDNUMBER_LENGTH);
        ActionCreators.changeCashbagCardNumber(value);
        e.preventDefault();
    }

    /**
     * 오케이 캐쉬백 비밀번호 입력시
     * @param  {[object]} e
     * @return {[void]}
     */
    onChangePassword(e){
        ActionCreators.changeCashbagPassword(e.target.value);
        e.preventDefault();
    }


    /**
     * 포인트 입력창값 변경시
     * @param  {[object]} e
     * @return {[void]}
     * 포인트 입력값이 전체값보다 클경우 최대치로 설정하고 입력모드가 텍스트 일경우 로컬넘버 포맷으로 변환함.
     */
    onChangePoint(e){
        let value = formatNumber(e.target.value),
            total = this.max();
            // 입력한 가격이, 내가쓸 수 있는 포인트 보다 크면
            // 포인트가 최대 가격이고
            // 그렇지 않으면 내가 적은 가격을 보여준다. 
        // value = (value - total) >= 0 && total || value;
        value = value >= total ? total : value

        this.setState({
            point: this.state.mode === 'text' &&  formatLocal(value) ||  value
        })
    }


     /**
     * 포인트입력창 포커스 인 핸들러
     * @param  {[object]} e
     * @return {[void]}
     * 키패드 입력모드를 숫자로 설정하고 초기값이 0일경우 입력하기 편하게 공백으로 처리
     */
    onFocusPoint(e){
        let value = formatNumber(e.target.value);
        this.setState({
            mode: 'number',
            point: ''
        });
    }

    /**
     * 포인트입력창 포커스 아웃 핸들러
     * @param  {[object]} e
     * @return {[void]}
     * 키패드 입력모드를 텍스트로 설정하고 입력된 값이 없을경우 0으로 세팅하고 있을경우는 로컬포맷으로 변환
     */
    onBlurPoint(e){
        let value = formatNumber(e.target.value);
        this.setState({
            mode: 'text',
            point: value > 0 && formatLocal(value) || '0'
        });
    }

    /**
     * 오케이케쉬백 포인트 요청
     * @return {[void]}
     * 입력폼 검사후 결과화면, 에러화면, 전송버튼 초기화 애니메이션과 프리로더 실행
     */
    onSubmit(){
        let { number, password } = this.state;
        number = words(number);
        if(number.length < MIN_CARDNUMBER_LENGTH){
            ActionCreators.openDialog(<Alert message={ ALLERT_INPUT_ERROR } />);
            return false;
        }
        if(!~~password.length){
            ActionCreators.openDialog(<Alert message={ ALLERT_INVALID_PASSWORD } />)
            return false;
        }

        this.show('loading', true);
        this.show('result', false);
        this.show('submit', false);
        this.show('error', false);
        $(ReactDOM.findDOMNode(this.preloader)).playLoading();
        ActionCreators.requestCashbagPoint(number, password);
    }

    /**
     * 카드번호 입력시 커서 위치 지정
     * @param  {[object]} e
     * @return {[void]}
     */
    onKeyUp(e){
        let count = e.target.value.length;
        e.target.setSelectionRange(count, count);
    }

    /**
     * 오케이 캐쉬백 포인트 모두 사용
     * @return {[void]}
     */
    onFullUse(){
        let point = !this.state.point && this.max() || 0;
        this.setState({ point: point  })
    }

    /**
     * 오케이 캐쉬백포인트가 결제금액보다 클경우 결제금액으로 반환.
     * @param   : {number}totalCost -  상품가격
                : {number}total - 내가 가지고 있는 총 포인트
     * @return {[number]}
     */
    max(){
        let { total, totalCost } = this.state;
        if (totalCost > 1000){
           return total > (totalCost - 1000) ? (totalCost - 1000) : (total)
        }
            return 0
        // return total > totalCost && totalCost || total;
    }

    /**
     * 각패털 애니메이션
     * @param  {[string]} type
     * @param  {[boolean]} active
     * @return {[void]}
     */
    show(type, active){
        TweenMax.to(this[type], 0.5, { height: active ? this[`${type}Height`] : 0, ease: Expo.easeOut });
    }

    /**
     * 카드화면 애니메이션
     * @param  {[boolean]} active
     * @return {[void]}
     */
    enter(active){
        TweenMax.to(this.holder, 0.5, {
            rotationY: active && -180 || 0,
            transformOrigin: 'center',
            transformStyle: 'preserve-3d',
            ease: Expo.easeOut
        });
        this.init = active;
        this.show('submit', true);
    }

    /**
     * 오케이캐쉬백 포인트 사용 취소
     * @return {[void]}
     */
    onCancel(){
        ActionCreators.closePopup();
        ActionCreators.changeCashbagPoint(0);
        this.setState({ point: 0 });
    }

    /**
     * 오케이캐쉬백 포인트 사용시
     * @return {void}
     */
    onConfirm(){
        if(!this.state.point){
            ActionCreators.openDialog(<Alert message={ ALLERT_INVALID_POINT } />);
            return false;
        }
        ActionCreators.closePopup();
        ActionCreators.changeCashbagPoint(formatNumber(this.state.point));
    }

    render() {
        return(
            <div className={ styles.container }>
                <div className={ styles.header }>
                    <div className={ styles.title }>OK캐쉬백 포인트</div>
                    <Button styles={ styles.closeButton }
                        mount={ this.props.active }
                        onEnterTween={{ rotation: 180, speed: 1, ease: Elastic.easeOut }}
                        onPressTween={{ scale: 0.9, speed:0 }}
                        onReleaseTween={{ scale: 1, speed:0 }}
                        onClick={ this.onCancel } />
                </div>
                <div className={ styles.contents }
                    ref={ ref => this.contents = ref }>
                    <div className={ styles.request }>
                        <div className={ styles.layout }
                            ref={ ref => this.layout = ref }>
                            <div className={ styles.holder }
                                ref={ ref => this.holder = ref }>
                                <div className={ `${styles.card} ${styles.front}` }
                                    ref={ ref => this.front = ref }
                                    onClick={ () => this.enter(false) }>
                                    <div className={ styles.cardnumber }>
                                        <form autoComplete="off">
                                            <input name="cardnumber"
                                                type="tel"
                                                placeholder="카드번호 16자리(-없이 입력)"
                                                value={ formatCreditcard(this.state.number) }
                                                onKeyUp={ this.onKeyUp }
                                                onChange={ this.onChangeNumber }
                                                maxLength="19"/>
                                        </form>
                                    </div>
                                    <div className={ styles.password }>
                                        <input name="password"
                                            type="password"                                           
                                            value={ this.state.password }                                     
                                            placeholder="온라인 비밀번호 입력"
                                            onChange={ this.onChangePassword }/>
                                    </div>
                                </div>
                                <div className={ `${styles.card} ${styles.back}` }
                                    ref={ ref => this.back = ref }>
                                    <div className={ styles.barcode } />
                                    <div>{ formatCreditcard(this.state.number) }</div>
                                    <div className={ styles.setting } onClick={ () => this.enter(false) } />
                                </div>
                            </div>
                        </div>
                        <div className={ styles.submit } ref={ ref => this.submit = ref }>
                            <button type="button" onClick={ this.onSubmit }>OK캐쉬백 포인트 조회</button>
                        </div>
                    </div>
                    <div className={ styles.loading } ref={ ref => this.loading = ref }>
                        <div className={ styles.preloader }
                            ref={ ref => this.preloader = ref }>
                            <div className='load_img' />
                        </div>
                        <div className="message">포인트를 조회 중입니다.</div>
                    </div>
                    <div
                        className={ styles.result }
                        ref={ ref => this.result = ref }>
                        <div className="remain">
                            <div className="remain1">
                                <span>잔여 포인트</span>
                                <span>{ formatLocal(this.state.cashbag) }</span>P    
                            </div> 
                            <span>/</span>
                            <div className="remain2">
                                <span>사용한도</span>
                                <span>{ formatLocal(this.max())  }</span>P    
                            </div>
                        </div>
                        <div className="spent">
                            <span>사용할 포인트</span>
                            <a className={ styles.use }
                                onClick={ this.onFullUse }>
                                { this.state.point < this.max() ? '최대사용' : '사용취소' }
                            </a>
                            <span>
                                <input type='tel'
                                    value={ formatLocal(this.state.point) }
                                    onChange={ this.onChangePoint }
                                    onFocus={ this.onFocusPoint }
                                    onBlur={ this.onBlurPoint }/>
                            </span>P
                        </div>
                    </div>
                    <div className={ styles.error }
                        ref={ ref => this.error = ref }>
                        <div className="image" />
                        <div className="message">{ this.state.error }</div>
                    </div>
                    <ul className={ styles.terms }
                        ref={ ref => this.terms = ref }>
                        <li>OK캐쉬백은 10원 단위로 사용이 가능하며, 총 결제 금액 중 최소 1,000원은 다른 결제 수단을 사용하셔야 합니다.</li>
                        <li>OK캐쉬백 웹회원 비밀번호는 OK캐쉬백 홈페이지에서 조회/변경이 가능합니다.</li>
                        <a href={ Config.CASHBAG_HOME_PAGE } target="_blank">OK캐쉬백 홈페이지 바로가기▶</a>
                        <li>카드번호가 잘못 입력되거나 유효하지 않은 상태일 경우 OK캐쉬백 잔액 확인이 되지 않을 수 있습니다.</li>
                        <li>보유포인트 부족 시, OK캐쉬백 사용이 불가하니 포인트 충전을 이용하시기 바랍니다.
                            <a href={ Config.CASHBAG_CHARGING_PAGE } target="_blank">충전하러 가기▶</a>
                        </li>
                    </ul>
                </div>
                <div className={ styles.footer }>
                    <Button styles={ `${styles.footerButton} ${styles.cancelButton}` }
                        onClick={ this.onCancel }
                        value={ '닫기' } />
                    <Button styles={ `${styles.footerButton} ${styles.confirmButton}` }
                        onClick={ this.onConfirm }
                        value={ '사용하기' } />
                </div>
            </div>
        )
    }
}


export default Container.create(CashbagViewer);