import React, { Component } from 'react';
import PropTypes from 'prop-types';
import equal from 'deep-equal';
import { Container } from 'flux/utils';
import { formatLoacal } from '../utils/UtilString';
import Coupon from './Coupon';
import styles from './overstockViewer.css';
import AppStore from '../stores/AppStore';
import ItemStore from '../stores/ItemStore';
import Overstock from './Overstock';
import ActionCreators from '../actions/ActionCreators';
import Button from '../ui/Button';

class OverstockViewer extends Component {

    static propTypes = {
        onConfirm: PropTypes.func,
        onCancel: PropTypes.func
    }

    static getStores(){
        return [ItemStore];
    }

    static calculateState(prevState){
        return {
            overstocks: ItemStore.overstocks,
        }
    }

    /**
     * 생성자
     * @param  {[object]} props
     * @return {[void]}
     */
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
        this.onConfirm = this.onConfirm.bind(this);
    }

    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    shouldComponentUpdate(nextProps, nextState) {
        const propsEqual = equal(this.props, nextProps),
            stateEqual = equal(this.state, nextState);
        return !propsEqual || !stateEqual;
    }

    /**
     * 재고수량 맟춤취소
     * @return {[void]}
     * 쿠폰 선택을 초기화
     */
    onCancel(){
        const { onCancel } = this.props;
        ActionCreators.closePopup();
        if(onCancel){
            onCancel();
        }
    }

    /**
     * 재고수량 맞춤요청
     * @return {[void]}
     */
    onConfirm(){
        const { onConfirm } = this.props;
        ActionCreators.closePopup();
        if(onConfirm){
            onConfirm();
        }
    }

    render() {
        let { overstocks } = this.state;
        return(
            <div className={ styles.container } >
                <div className={ styles.header }>
                    <div className={ styles.title }>알림</div>
                    <Button styles={ styles.closeButton }
                        mount={ this.props.active }
                        onEnterTween={{ rotation: 180, speed: 1, ease: Elastic.easeOut }}
                        onPressTween={{ scale: 0.9, speed:0 }}
                        onReleaseTween={{ scale: 1, speed:0 }}
                        onClick={ this.onCancel } />
                </div>
                 <div className={ styles.wrapper }>
                    <div className={ styles.figure }>
                        <div className={ styles.figuretitle }>최대 주문 가능 수량</div>
                        <div className={ styles.figurecaption }>재고가 부족하여 최대 수량으로 주문합니다</div>
                    </div>
                    <div className={ styles.contents }>
                       {
                            overstocks.map((item, i) =>{
                                return(<Overstock
                                    data={ item }
                                    key={ `item-${i}` }
                                    index={ i }/>);
                            })
                       }
                    </div>
                    <div className={ `${styles.confirmImage} ${ overstocks.length > 3 && styles.hide || ''}` }>최대 수량으로 주문하시겠습니까?</div>
                </div>
                <div className={ styles.footer }>
                    <Button styles={ `${styles.footerButton} ${styles.cancelButton}` }
                        onClick={ this.onCancel }
                        value={ '취소' } />
                    <Button styles={ `${styles.footerButton} ${styles.confirmButton}` }
                        onClick={ this.onConfirm }
                        value={ '확인' } />
                </div>
            </div>
        )
    }
}

export default Container.create(OverstockViewer);