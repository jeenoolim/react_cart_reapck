import React, { Component } from 'react';
import PropTypes from 'prop-types';
import equal from 'deep-equal';
import { Container } from 'flux/utils';
import ItemCart from './ItemCart';
import styles from './itemViewer.css';
import AppStore from '../stores/AppStore';
import ItemStore from '../stores/ItemStore';
import ActionCreators from '../actions/ActionCreators';
import ItemViewPannel from './ItemViewPannel';
import Alert from '../ui/Alert';
import Messages from '../dialog/Messages';
import Checkbox from '../ui/Checkbox';
import Button from '../ui/Button';

class ItemViewer extends Component {

    static getStores(){
        return [AppStore, ItemStore];
    }

    static calculateState(prevState){
        return {
            session: AppStore.session,
            items: ItemStore.items,
            toggle: AppStore.toggle,
            checkeds: ItemStore.checkeds,
            disableds: ItemStore.disableds,
            cost: ItemStore.cost,
            noStorageDumItemList: AppStore.noStorageDumItemList,
        }
    }

    static propTypes = {
        mode: PropTypes.string.isRequired,
    }

    /**
     * 생성자
     * @param  {[object]}
     * @return {[void]}
     */
    constructor(props) {
        super(props);
        this.childs = [];
        this.onDelete = this.onDelete.bind(this);
        this.onChecked = this.onChecked.bind(this);
        this.onChildUpdated = this.onChildUpdated.bind(this);
    }


    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    shouldComponentUpdate(nextProps, nextState) {
        const propsEqual = equal(this.props, nextProps),
            stateEqual = equal(this.state, nextState);
            console.log(propsEqual, stateEqual);
        return !propsEqual || !stateEqual;
    }


    /**
     * 자식 아이템 컴포넌트 변경시
     * @param  {[object]} child
     * @return {[void]}
     * 배열 형태로 관리
     */
    onChildUpdated(child){
        this.childs[child.index] = child;//child.checked && child.node || null;
        // this.childs = this.childs.filter(child => child !== null);
    }

    /**
     * 컴포넌트가 업데이트 될때
     */
    componentDidUpdate(prevProps, prevState){     
    }



    /**
     * 선택삭제시
     * @return {[void]}
     * 알림창 띄운후 삭제 애니메이션
     */
    onDelete(type){
        ActionCreators.openDialog(<Alert
            message={ Messages.DELETE_SELECT_ITEM }
            onConfirm={ () => this.tween(type)}/>);
    }


    /**
     * 토글 전체 선택
     * @return {[void]}
     */
    onChecked(){
        let items = this.state.items;
        if(!items.length){
            return false;
        }
        
        ActionCreators.updateItem(items, Array(items.length).fill({ checked: !this.state.toggle }));
    }

    /**
     * 삭제되는 애니메이션
     * @return {[type]} [description]
     * straggerTo, staggerFrom 일정 시간의 딜레이를 두고 리스트를 순차적으로 실행.
     * 대상, 시간, 속성, 딜레이, 애니메이션 모두 완료
     */
    tween(type){
        const list = this.childs.filter(child => child[type]).map(child => child.node);
        TweenMax.staggerTo(list, 0.2, { x: '110%', ease: Expo.easeOut }, 0.1, () => {
            TweenMax.set(list, { x: '0%' });
            ActionCreators.deleteItem(this.state[`${type}s`]);
        })
    }

    /**
     * 렌더링
     * @return {[void]}
     */
    render() {
        const { items, checkeds, disableds, toggle, session, noStorageDumItemList } = this.state,
            { mode } = this.props, checkCount = checkeds.length, checkTotal = items.length, disabledCount = disableds.length;
        return (
            <div className={ `${styles.container} ${ mode !== 'cart' && styles.hide || '' }` }>
                <div className={ styles.controller }>
                    <Checkbox
                        styles={ `${ styles.checkbox } ${ toggle && styles.checked || '' }` }
                        checked={ toggle }
                        onChecked={ this.onChecked }/>
                    <div className={ styles.label }>{ `전체선택( ${checkCount} / ${checkTotal} )` }</div>
                    <Button
                        styles={ `${ styles.deleteButton } ${ (!session || !checkCount) && styles.hide || '' }` }
                        value={ '선택삭제'}
                        onClick={ () => this.onDelete('checked') } />
                </div>
                <ItemViewPannel items={ items }/>
                    { items.map((item, i) => {
                        return(<ItemCart
                            noStorageDumItemList = {noStorageDumItemList}
                            data={ item }
                            key={ `item-${i}` }
                            onChildUpdated = { this.onChildUpdated }
                            index={ i }/>);
                        })
                    }
                <div className={ styles.controller }>
                    <Button
                        styles={ `${ styles.deleteButton } ${ (!session || !disabledCount) && styles.hide || '' }` }
                        value={ '품절/판매종료 삭제' }
                        onClick={ () => this.onDelete('disabled') } />
                    <Checkbox
                        styles={ `${ styles.checkbox } ${ toggle && styles.checked || '' } ${ disabledCount && styles.hide || '' }` }
                        checked={ toggle }
                        onChecked={ this.onChecked }/>
                    <div className={ `${styles.label} ${ disabledCount && styles.hide || '' }` }>
                        { `전체선택( ${checkCount} / ${checkTotal} )` }
                    </div>
                    <Button
                        styles={ `${ styles.deleteButton } ${ (!session || !checkCount) && styles.hide || '' }` }
                        value={ '선택삭제'}
                        onClick={ () => this.onDelete('checked') } />
                </div>
            </div>
        )
    }
}


export default Container.create(ItemViewer);
