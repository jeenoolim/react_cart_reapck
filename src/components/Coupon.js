import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classNames';
import styles from './coupon.css';
import Radio from '../ui/Radio';
import Button from '../ui/Button';
import CouponTargetViewer from './CouponTargetViewer';
import { formatLocal } from '../utils/UtilString';
import ActionCreators from '../actions/ActionCreators';

export default class Coupon extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        index: PropTypes.number,
        selected: PropTypes.number,
        unfolded: PropTypes.number,
    }

    constructor(props) {
        super(props);
        this.onSelect = this.onSelect.bind(this);
        this.onClick = this.onClick.bind(this);
    }


    onSelect(e){
        if(this.props.data.disabled){
            return false;
        }
        ActionCreators.changeCoupon(this.props.index);
        e.preventDefault();
    }

    onClick(e){
        ActionCreators.unfoldCoupon(this.props.index);
        e.preventDefault();
        e.stopPropagation();
    }

    render() {
        let { data, index, selected, unfolded } = this.props;

        selected = index ===  selected;
        unfolded = index === unfolded;

        return(
            <div className={ styles.container }>
                <div className={ styles.header } onClick={ this.onSelect }>
                    <Radio styles={ `${styles.radio} ${selected && styles.selected || ''}` }/>
                </div>
                <div className={ classNames(
                    styles.body,
                    data.disabled && styles.disabledBorder || '') }>
                    <div className={ styles.top }
                        onClick={ this.onSelect }>
                        <div className={ classNames(
                            styles.left,
                            data.disabled && styles.disabledColor || '') }>
                            <em className={ classNames(
                                    styles.name,
                                    data.free && styles.free || '',
                                    data.free && data.disabled && styles.disabledBg || '' )}>
                                { data.free && data.name || formatLocal(data.name) }
                            </em>
                            <div className={ styles.label }>
                                { data.label }
                            </div>
                        </div>
                        <div className={ styles.right }>
                            <div className={ classNames(
                                styles.explain,
                                data.disabled && styles.disabledColor || '') }>
                                { data.explain }
                            </div>
                            <div className={ styles.terms }>({ data.terms }원 이상 구매시 사용가능)</div>
                            <div className={ styles.period }>{ data.period }</div>
                        </div>
                    </div>
                    {/* 
                    <CouponTargetViewer data={ data }
                        open={ unfolded }
                        onClick={ this.onClick }/>
                    */}

                </div>
            </div>
        )
    }
}

