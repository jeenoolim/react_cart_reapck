import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './paymentOverview.css';
import deepEqual from 'deep-equal';
import { formatLocal } from '../utils/UtilString';
import { numberTween } from '../utils/UtilMotion';
import { domStyle } from '../utils/UtilElement';
import Button from '../ui/Button';
 class PaymentOverview extends Component {

    static propTypes = {
        open: PropTypes.bool,
        mode: PropTypes.string,
        cost: PropTypes.number,
        shippingTip: PropTypes.string,
        shippingCost: PropTypes.number,
        onToggle: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        this.cost = { value: 0 };
        this.shippingCost = { value: 0 };
        this.state = {
            cost: 0,
            shippingCost: 0,
        };
    }


    componentWillReceiveProps(nextProps) {
        numberTween(this.cost, Number(nextProps.cost), (tween) => {
            this.setState({ cost: tween.value.toFixed(0) });
        })

        numberTween(this.shippingCost, Number(nextProps.shippingCost), (tween) => {
            this.setState({ shippingCost: tween.value.toFixed(0) });
        })

        TweenMax.set(this.arrowButton.dom, { rotation: nextProps.open * 180 });
    }

    render() {
        const { shippingTip, toggle, mode } = this.props,
            { cost, shippingCost } = this.state;
        return (
            <div className={ styles.container }>
                <div className={ styles.row }>
                    <div className={ styles.left }>주문금액</div>
                    <em className={ styles.right }>{ formatLocal(cost) }</em> 원
                </div>
               <div className={ mode === 'cart' ? styles.row : styles.row }>
                    <div className={ styles.left }>
                        <span>배송비</span>
                        <span className={ styles.tip }>({ shippingTip })</span>
                    </div>
                    <em className={ styles.right }>{ formatLocal(shippingCost) }</em> 원
                </div>
                 <div className={ styles.row }>
                    <div className={ styles.left }>할인</div>
                    <div className={ styles.right } >
                        <Button styles={ styles.arrowButton }
                            onClick={ this.props.onToggle }
                            ref={ ref => this.arrowButton = ref }/>
                    </div>
                </div>
            </div>
        )
     }
 }

 export default PaymentOverview;


