import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Config from '../Config';
import { domAttr, domStyle } from '../utils/UtilElement';
import styles from './couponTargetViewer.css';
import Button from '../ui/Button';
import equal from 'deep-equal';
import SlidePannel from '../ui/SlidePannel';

export default class CouponTargetViewer extends Component {
    static propTypes = {
        data: PropTypes.object,
        onClick: PropTypes.func,
        open: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    shouldComponentUpdate(nextProps, nextState) {
        const propsEqual = equal(this.props, nextProps),
            stateEqual = equal(this.state, nextState);
        return !propsEqual || !stateEqual;
    }

    onClick(e){
        this.props.onClick(e);
    }

    render() {
        let { data, open } = this.props,
            targets = data.targets,
            count = targets.length;
        return (
            <div className={ `${ styles.container } ${ !count && styles.hide || '' }` }>
                <div className={ styles.header } >
                    <Button styles={ `${ styles.arrowButton } ${ open && styles.arrowDown || styles.arrowUp }` }
                        onClick={ this.onClick }
                        value="적용상품보기"/>
                </div>
                <SlidePannel open={ open }
                    data={
                        <div className={ styles.pannel }>
                            <div className={ styles.title }>쿠폰 적용 상품</div>
                            <ul className={ styles.list }>
                                {
                                    targets.map((item, i) => {
                                        return <li className={ `${ styles.item }` }
                                            key={ `coupon-item${i}` }>
                                            <a className={ item.active && styles.active || '' }
                                                href={ `${ Config.ITEM_LINK_PATH }${ item.code }` }>
                                                { item.name }
                                            </a>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    } />
            </div>

        );
    }
}
