import React, { Component } from 'react';
import deepEqual from 'deep-equal';
import PropTypes from 'prop-types';
import Button from '../ui/Button';
import Terms from './Terms';
import Agreement from './Agreement';
import styles from './footer.css';
import Radio from '../ui/Radio';
import Alert from '../ui/Alert';
import ActionCreators from '../actions/ActionCreators';

export default class Footer extends Component{
	static propTypes = {
		mode: PropTypes.string.isRequired,
		checker: PropTypes.func.isRequired, //reservingChecker
		items: PropTypes.array,
		checkedItemsCode: PropTypes.array,		
		adults: PropTypes.array,
		forward: PropTypes.func.isRequired,
		backward: PropTypes.func.isRequired,		
	}

	constructor(props) {
		super(props);
 		this.popup = true;
		this.onChange = this.onChange.bind(this);	
	}


	shouldComponentUpdate(nextProps, nextState) {
		return !deepEqual(this.props, nextProps) || !deepEqual(this.state, nextState);
	}

	// '일반 상품', '예약배송 전용상품'. '일반배송+예약배송 전용상품' 라디오 버튼 클릭시 발생하는 이벤트
	onChange(e){
		const { items } = this.props;
		let value = e.target.value; // 선택한 radio 버튼의 value값
		// 장바구니 아이탬을 순환시켜서 특정 조건에 따라 배열을 하나 생성 한다.
		let props = items.map(function(item){
			let reservaionStatus = item.reservation; 
			 // 선택한 radio 버튼의 value에 따라, 
			 switch(value){
				/***********************************************************
				* @name			: {variable}reservaionStatus
				* @description	: itemStore.js reservation getter로 item의 reservation 상태값을 나타냄
								: 'only', 'normal', 'unable'이 있다.
				*************************************************************/
				// 선택한 값에 따라
				case 'normal' :
					// reservaionStatus가 'normal' 또는 'unable'일 경우 cehcked: true, 
					return {checked: reservaionStatus === 'normal' || reservaionStatus === 'unable'};
				case 'only' : 
					// reservaionStatus가 'only'일 경우 cehcked: true, 
					return {checked: reservaionStatus === 'only'};
				case 'only_plus_normal' : 
					// reservaionStatus가 'only' 또는 'normal'일 경우 cehcked: true, 
					return {checked: reservaionStatus === 'only' ||  reservaionStatus === 'normal'};
			}
		})
	
		this.popup = false;
		// 위에 switch 문에 따라 선택된 item의 cehcked속성을 업데이트 시킨다. 
		ActionCreators.updateItem(items, props);

	}

	render() {
		const { mode, items, checker, forward, backward, adults, checkedItemsCode } = this.props, 
			cart = mode === 'cart';
		return(
			<div>
				<div className={ (cart && checker('able', 'only') === 'mixed') && styles.radioContainer || styles.hide }>
					<Radio
						styles={ styles.radio  }
						onChange={ this.onChange }
						checked={ checker('current', 'only') === 'none' }
						name="reserve"
						label="일반 상품"
						description="만 주문합니다."
						value="normal" />
					<Radio
						styles={ styles.radio }
						onChange={ this.onChange }
						checked={ checker('current', 'only') === 'pure' }
						name="reserve"
						label="예약배송 전용상품"
						description="만 주문합니다."
						value="only" />
					{/*}
					<Radio
						styles={ styles.radio }
						onChange={ this.onChange }
						checked={ checker('current', 'only') === 'mixed' }
						name="reserve"
						label="예약배송 전용상품 + 일반상품"
						description="예약배송 선택일에 받기"
						value="only_plus_normal" />
					*/}
				</div>
				<div className={ !adults.length && styles.hide || styles.terms }>
					<Terms />
				</div>
				<Agreement items={ items } mode={ mode }/>
				<div className={styles.buttonContainer }>
					<Button styles={ `
							${ styles.button }
							${ cart && styles.reservation || styles.cancel }
							${ (cart && checker('current', 'only') === 'none') && styles.hide || '' }` }
							onClick={ backward }
							value={ cart && '예약배송' || '취소하기' } />
					<Button styles={ `
							${ styles.button }
							${ styles.order }
							${ (cart && checker('current', 'only') === 'none') ?  '' : styles.gap }
							${ (cart && checker('current', 'only') !== 'none') && styles.hide || '' }
							${ cart && 'btn_ct_order' || '' } ` }							
							onClick={ forward }
							value={ cart && '주문하기' || '결제하기' } 
							checkedItemsCode = {cart &&  checkedItemsCode || null }/>
				</div>
			</div>
		)
	}
}



