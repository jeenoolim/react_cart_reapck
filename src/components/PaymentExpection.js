import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatLocal } from '../utils/UtilString';
import { numberTween } from '../utils/UtilMotion';
import styles from './paymentExpection.css';

export default class PaymentExpection extends Component{

    static propTypes = {
        payment: PropTypes.number,
        mileage: PropTypes.number,
    }

    constructor(props) {
        super(props);
        this.payment = { value: 0 };
        this.mileage = { value: 0 };
        this.state={
            payment: 0,
            mileage: 0,
        }
    }


    componentWillReceiveProps(nextProps) {
        numberTween(this.payment, nextProps.payment, (tween) => {
            this.setState({ payment: tween.value.toFixed(0) });
        })
        numberTween(this.mileage, nextProps.mileage, (tween) => {
            this.setState({ mileage: tween.value.toFixed(0) });
        })
    }

    render() {
        const { payment, mileage } = this.state;
        return (
            <div className={ styles.container }>
                <div className={ styles.wrapper }>
                    <div className={ styles.row }>
                        <div className={ styles.left }>결제예상금액</div>
                        <div className={ styles.right }>
                            <em className={ this.props.payment && styles.orange || '' }>{ formatLocal(payment) }</em>원
                            <span className={ styles.mileage }> (예상적립금 { formatLocal(mileage) }P)</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
