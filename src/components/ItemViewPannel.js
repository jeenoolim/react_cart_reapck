import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './itemViewPannel.css';
import Button from '../ui/Button';
import Config from '../Config';

export default class ItemViewPannel extends Component {
    static propTypes = {
        items: PropTypes.array,
    };

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }


    onClick(){
        window.location.href = Config.HOME_LINK_PATH;
    }

    render() {
        return (
            <div className={ `${styles.container} ${ this.props.items.length && styles.hide }` }>
                <div className={ styles.wrap }>
                    <div>장바구니에</div>
                    <div>담긴 상품이 없습니다.</div>
                    <Button styles={ styles.linkButton }
                        value="추천상품 보러가기"
                        onClick={ this.onClick }/>
                </div>
            </div>
        );
    }
}
