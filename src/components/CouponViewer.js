import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'flux/utils';
import { formatLoacal } from '../utils/UtilString';
import equal from 'deep-equal';
import Coupon from './Coupon';
import styles from './couponViewer.css';
import CouponStore from '../stores/CouponStore';
import AppStore from '../stores/AppStore';
import ItemStore from '../stores/ItemStore';
import ActionCreators from '../actions/ActionCreators';
import Button from '../ui/Button';

class CouponViewer extends Component {

    static getStores(){
        return [CouponStore];
    }

    static calculateState(prevState){
        return {
            items: ItemStore.items,
            toggle: AppStore.toggle,
            cost: ItemStore.cost,
            point: ItemStore.point,
            shippingCost: ItemStore.shippingCost,
            coupons: CouponStore.coupons,
            count: CouponStore.count,
            selected: CouponStore.selected,
            unfolded: CouponStore.unfolded,
        }
    }

    /**
     * 생성자
     * @param  {[object]} props
     * @return {[void]}
     */
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
        this.onConfirm = this.onConfirm.bind(this);
    }

    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    shouldComponentUpdate(nextProps, nextState) {
        const stateEqual = equal(this.state, nextState);
        return !stateEqual;
    }

    /**
     * 쿠폰선택 취소
     * @return {[void]}
     * 쿠폰 선택을 초기화
     */
    onCancel(){
        ActionCreators.closePopup();
        ActionCreators.changeCoupon(-1);
    }

    /**
     * 쿠폰선택 완료
     * @return {[void]}
     */
    onConfirm(){
        ActionCreators.closePopup();
    }

    render() {
        let { coupons, count, selected, unfolded } = this.state,
            { totalprice, usedPoint, shipping } = this.props;
        return(
            <div className={ styles.container } >
                <div className={ styles.header }>
                    <div className={ styles.title }>쿠폰 선택</div>
                    <Button styles={ styles.closeButton }
                        mount={ this.props.active }
                        onEnterTween={{ rotation: 180, speed: 1, ease: Elastic.easeOut }}
                        onPressTween={{ scale: 0.9, speed:0 }}
                        onReleaseTween={{ scale: 1, speed:0 }}
                        onClick={ this.onCancel } />
                </div>
                <div className={ styles.contents }>
                   {
                        coupons.map((coupon, i) =>{
                            return(
                                <Coupon key={ `coupon${i}` }
                                    data={ coupon }
                                    index={ i }
                                    selected={ selected }
                                    unfolded={ unfolded }
                                    totalprice={ totalprice }
                                    usedPoint={ usedPoint }
                                    selectedIndex={ this.state.selectedIndex }
                                    viewIndex={ this.state.viewIndex }
                                    shipping={ shipping }
                                    items={ this.props.items }/>
                            );
                        })
                   }
                </div>
                 <div className={ styles.footer }>
                    <Button styles={ `${styles.footerButton} ${styles.cancelButton}` }
                        onClick={ this.onCancel }
                        value={ '선택취소' } />
                    <Button styles={ `${styles.footerButton} ${styles.confirmButton}` }
                        onClick={ this.onConfirm }
                        value={ '선택완료' } />
                </div>
            </div>
        )
    }
}

export default Container.create(CouponViewer);