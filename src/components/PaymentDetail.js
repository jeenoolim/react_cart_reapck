import React, { Component } from 'react';
import { formatLocal, formatNumber } from '../utils/UtilString';
import { numberTween } from '../utils/UtilMotion';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import Messages from '../dialog/Messages';
import Checkbox from '../ui/Checkbox';
import Button from '../ui/Button';
import Alert from '../ui/Alert';
import CouponViewer from './CouponViewer';
import CashbagViewer from './CashbagViewer';
import SlidePannel from '../ui/SlidePannel';
import styles from './paymentDetail.css';
import ActionCreators from '../actions/ActionCreators';



export default class PaymentDetail extends Component {

    static propTypes={
        session: PropTypes.object,
        totalCost: PropTypes.number,
        pointTotal: PropTypes.number,
        pointSpent: PropTypes.number,
        pointCashbag: PropTypes.number,
        couponCount: PropTypes.number,
        specialDiscount: PropTypes.number,
        totalDiscount: PropTypes.number,
        couponData: PropTypes.object,
    }

    /**
     * 생성자
     * @param  {[object]} props
     * @return {[void]}
     */
    constructor(props) {
        super(props);
        this.special = { value: 0 };
        this.total = { value: 0 };
        this.state={
            spent: '0',
            special: 0,
            total: 0,
         }

        this.openCoupon = this.openCoupon.bind(this);
        this.openCashbag = this.openCashbag.bind(this);
        this.onBlurPoint = this.onBlurPoint.bind(this);
        this.onFocusPoint = this.onFocusPoint.bind(this);
        this.onFullUse = this.onFullUse.bind(this);
        this.onChangePoint = this.onChangePoint.bind(this);
    }

    /**
     * 속성 변경시 숫자 애니메이션
     * @param  {[object]} nextProps
     * @return {[void]}
     */
    componentWillReceiveProps(nextProps){
        this.setState({ spent: formatLocal(nextProps.pointSpent) });
        numberTween(this.special, Number(nextProps.specialDiscount), (tween) => {
            this.setState({ special: tween.value.toFixed(0) });
        })

        numberTween(this.total, Number(nextProps.totalDiscount), (tween) => {
            this.setState({ total: tween.value.toFixed(0) });
        })
    }

    /**
     * 쿠폰팝업
     * @return {[void]}
     */
    openCoupon(){
        if(formatNumber(this.state.spent)){
            ActionCreators.openDialog(
                <Alert message={ Messages.INVALID_COUPON_POINT }
                    onConfirm={ () => {
                        ActionCreators.changePoint(0) ;
                        ActionCreators.openPopup(<CouponViewer />);
                    }} />);
            return false;
        }
        if(this.props.pointCashbag){
             ActionCreators.openDialog(
                <Alert message={ Messages.INVALID_CASHBAG_COUPON }
                    onConfirm={ () => {
                        ActionCreators.changeCashbagPoint(0) ;
                        ActionCreators.openPopup(<CouponViewer />);
                    }} />);
            return false;
        }
        ActionCreators.openPopup(<CouponViewer />);
    }


    /**
     * 캐시백 팝업
     * @return {[void]}
     */
    openCashbag(){
        if(this.props.couponData){
            ActionCreators.openDialog(
                <Alert message={ Messages.INVALID_COUPON_CASHBAG }
                    onConfirm={ () => {
                        ActionCreators.changeCoupon(-1);
                        ActionCreators.openPopup(<CashbagViewer />);
                    }} />);
            return false;
        }
        if(formatNumber(this.state.spent)){
            ActionCreators.openDialog(
                <Alert message={ Messages.INVALID_POINT_CASHBAG }
                    onConfirm={ () => {
                        ActionCreators.changePoint(0) ;
                        ActionCreators.openPopup(<CashbagViewer />);
                    }} />);
            return false;
        }
        ActionCreators.openPopup(<CashbagViewer />);
    }

    /**
     * 마일리지 사용시
     * @param  {[number]}
     * @return {[void]}
     * 쿠폰과 캐쉬백 포인트가 사용중일때 초기화 경고창
     */
    changePoint(point){
        if(this.props.couponData){
            ActionCreators.openDialog(
                <Alert message={ Messages.INVALID_COUPON_POINT }
                    onConfirm={ () => {
                        ActionCreators.changeCoupon(-1);
                        ActionCreators.changePoint(point);
                    }} />);
            return false;
        }
        if(this.props.pointCashbag){
             ActionCreators.openDialog(
                <Alert message={ Messages.INVALID_CASHBAG_COUPON }
                    onConfirm={ () => {
                        ActionCreators.changeCashbagPoint(0);
                        ActionCreators.changePoint(point);
                    }} />);
            return false;
        }
        ActionCreators.changePoint(point);
    }

    /**
     * 포인트 입력창값 변경시
     * @param  {[object]} e
     * @return {[void]}
     * 포인트 입력값이 전체값보다 클경우 최대치로 설정하고 입력모드가 텍스트 일경우 로컬넘버 포맷으로 변환함.
     */
    onChangePoint(e){
        let value = formatNumber(e.target.value),
            total = this.max();
        value = (value > total) && total || value;
        ActionCreators.changePoint(value);
    }

    /**
     * 포인트입력창 포커스 인 핸들러
     * @param  {[object]} e
     * @return {[void]}
     * 키패드 입력모드를 숫자로 설정하고 초기값이 0일경우 입력하기 편하게 공백으로 처리
     */
    onFocusPoint(e){
        this.setState({ spent: '' });
    }

    /**
     * 포인트입력창 포커스 아웃 핸들러
     * @param  {[object]} e
     * @return {[void]}
     * 키패드 입력모드를 텍스트로 설정하고 입력된 값이 없을경우 0으로 세팅하고 있을경우는 로컬포맷으로 변환
     */
    onBlurPoint(e){
        let value = formatNumber(e.target.value);
        this.setState({ spent: value > 0 && formatLocal(value) || '0' });
        this.changePoint(value);       
    }

    /**
     * 포인트 모두사용 토글버튼 핸들러
     * @return {[void]}
     * 토글기준은 사용할수 있는 포인트가 있는경우 와 없는경우
     */
    onFullUse(){
        let spent = !formatNumber(this.state.spent) && this.max() || 0;
        this.changePoint(spent);
    }


    /**
     * 전체 포인트에서 사용한 포인트를 차감하고 반환
     * @return {[number]}
     */
    remainPoint(){
        return this.props.pointTotal - formatNumber(this.state.spent);
    }

    /**
     * 가용 포인트 반환
     * @return {[number]}
     * 사용할수 있는 포인트가 지불 금액보다 클경우 지불금액으로 맞춤.
     */
    max(){
        let { pointTotal, totalCost } = this.props;
        return pointTotal > totalCost && totalCost || pointTotal;
    }


    /**
     * 포인트 토글버튼 라벨
     * @return {[string]}
     */
    pointButtonLabel(){
        return !formatNumber(this.state.spent) && '모두사용' || '사용취소';
    }

    /**
     * 쿠폰 선택라벨
     * @return {[string]}
     * 선택된 쿠폰 이름을 반환함
     */
    couponButtonLabel(){
        let data = this.props.couponData;
        return data && `${ data.label }(${ data.name })` || '쿠폰을 선택하세요';
    }

    /**
     * 렌더링
     * @return {[void]}
     */
    render() {
        const { session, pointTotal, pointCashbag, couponCount, couponData, totalCost, specialDiscount, totalDiscount } = this.props,
            { spent, total, special } = this.state;
        return(
            <div className={ styles.container }>
                <div className={ styles.row }>
                    <div className={ styles.left }>
                        <Checkbox styles={ `${styles.checkbox}
                            ${ couponData && styles.checked || ''}` }/>
                        <div className={ styles.subject } >쿠폰
                            <span className={ !couponCount && styles.disabled || '' }>(보유쿠폰
                                <em className={ styles.green }>
                                    { couponCount }장
                                </em>)
                            </span>
                        </div>
                    </div>
                    <div className={ styles.right }>
                        <span className={ couponCount && styles.disabled }>
                            <em>0</em>
                            <span className={ styles.unit }>장</span>
                        </span>
                        <Button styles={
                            `${ styles.couponButton }
                            ${ couponData && styles.orange || '' }
                            ${ !couponCount && styles.disabled || '' }`}
                            onClick={ this.openCoupon }
                            value={ this.couponButtonLabel() } />
                    </div>
                </div>
                 <div className={ styles.row }>
                    <div className={ styles.left }>
                        <Checkbox styles={ `${styles.checkbox}
                        ${ formatNumber(spent) && styles.checked || ''}`} />
                        <div className={ styles.subject }>포인트
                            <span className={ pointTotal || styles.disabled }>(
                                <em className={ styles.green }>
                                    { formatLocal(this.remainPoint()) }
                                </em>P)
                            </span>
                        </div>
                        <Button styles={ `${styles.pointButton}
                            ${ !pointTotal && styles.disabled || '' }` }
                            onClick={ this.onFullUse }
                            value={ this.pointButtonLabel() } />
                    </div>
                    <div className={ styles.right }>
                        <span className={ pointTotal && styles.disabled }>
                            <em>0</em>
                            <span className={ styles.unit }>P</span>
                        </span>
                        <div className={ `${styles.inputBox}
                            ${ !pointTotal && styles.disabled || '' }
                            ${ formatNumber(spent) && styles.orange || '' }` }>
                            <input type='tel'
                                className={ styles.input }
                                onChange={ this.onChangePoint }
                                value={ spent }
                                onBlur={ this.onBlurPoint }
                                onFocus={ this.onFocusPoint }/>P
                        </div>
                    </div>
                </div>
                <div className={ styles.row }>
                    <div className={ styles.left }>
                        <Checkbox styles={ `${styles.checkbox}
                            ${ pointCashbag && styles.checked || ''}`} />
                        <div className={ styles.subject }>OK캐쉬백포인트</div>
                        <Button styles={ `${styles.cashbagButton}
                            ${ totalCost && session ? '' : styles.disabled }` }
                            onClick={ this.openCashbag }
                            value={ '조회' } />
                    </div>
                    <div className={ styles.right }>
                        <em className={ pointCashbag && styles.orange || '' }>
                            { formatLocal(pointCashbag) }
                        </em>
                        <span className={ styles.unit }>P</span>
                    </div>
                </div>
                <div className={ styles.row }>
                    <div className={ styles.left }>
                        <Checkbox styles={ `${styles.checkbox}
                            ${ specialDiscount && styles.checked || ''}`} />
                        <div className={ styles.subject }>특별할인</div>
                    </div>
                    <div className={ styles.right }>
                        <em className={ specialDiscount && styles.orange || '' }>
                            { formatLocal(special) }
                        </em>
                       <span className={ styles.unit }>원</span>
                    </div>
                </div>
                <div className={ `${styles.row} ${styles.total}` }>
                    <div className={ styles.left }>
                        <div className={ styles.subject }>총 할인금액</div>
                    </div>
                    <div className={ styles.right }>
                        <em className={ totalDiscount && styles.orange || '' }>
                            { `${ ~~total && '-' || '' }${formatLocal(total)}` }
                        </em>
                        <span className={ styles.unit }>원</span>
                    </div>
                </div>
            </div>

        )
    }
}

