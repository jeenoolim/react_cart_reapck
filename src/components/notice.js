import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Notice extends Component {

    static propTypes = {
        reservation: PropTypes.bool.isRequired,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: this.props.reservation ? 'none' : 'block' }} className="warring_prod_box warring_prod_box2">
                <h1><span className="sp1">예약배송이 불가합니다.</span></h1>
                <p>고객님 장바구니에 <span className="sp2">예약배송이 불가한 상품이 포함</span>되어 있습니다.</p>
                <p>예약 배송이 불가한 상품의 경우 상품 페이지 상단에 예약배송 불가에 대해 안내가 되어있으니,</p>
                <p>해당 상품 외 <span className="sp2">예약배송을 원하시는 상품은 별도로 주문</span>해주세요.</p>
            </div>
        );
    }
}


