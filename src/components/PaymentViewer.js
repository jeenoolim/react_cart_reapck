import React, { Component } from 'react';
import { Container } from 'flux/utils';
import ReactDOM from 'react-dom';
import equal from 'deep-equal';
import PropTypes from 'prop-types';
import SlidePannel from '../ui/SlidePannel';
import styles from './paymentViewer.css';
import PaymentDetail from './PaymentDetail';
import PaymentOverview from './PaymentOverview';
import PaymentExpection from './PaymentExpection';
import AppStore from '../stores/AppStore';
import ItemStore from '../stores/ItemStore';
import CouponStore from '../stores/CouponStore';
import CashbagStore from '../stores/CashbagStore';
import ActionCreators from '../actions/ActionCreators';

class PaymentViewer extends Component {

    static getStores(){
        return [AppStore, ItemStore, CouponStore, CashbagStore]
    }

    static calculateState(prevState){
        return {
            session: AppStore.session,
            mode: AppStore.mode,
            items: ItemStore.items,
            toggle: AppStore.toggle,
            payment: ItemStore.payment,
            cost: ItemStore.cost,
            totalCost: ItemStore.totalCost,
            pointTotal: ItemStore.pointTotal,
            pointSpent: ItemStore.pointSpent,
            pointCashbag:CashbagStore.spent,
            shippingTip: ItemStore.shippingTip,
            shippingCost: ItemStore.shippingCost,
            specialDiscount: ItemStore.specialDiscount,
            totalDiscount: ItemStore.totalDiscount,
            mileage: ItemStore.mileage,
            coupons: CouponStore.coupons,
            couponCount: CouponStore.count,
            couponData: CouponStore.current,
        }
    }


    constructor(props){
        super(props);
        this.state = { open: true }
        this.onToggle = this.onToggle.bind(this);
    }

    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    // shouldComponentUpdate(nextProps, nextState) {
    //     const stateEqual = equal(this.state, nextState);
    //     return !stateEqual;
    // }

    onToggle(){
        this.setState({ open: !this.state.open });
    }

    render() {
        return (
            <div className={ styles.container }>
                <div className={ styles.wrapper }>
                    <PaymentOverview
                        cost={ this.state.cost }
                        mode={ this.state.mode }
                        shippingTip={ this.state.shippingTip }
                        shippingCost={ this.state.shippingCost }
                        onToggle={ this.onToggle }
                        open={ this.state.open }/>
                    <SlidePannel open={ this.state.open }
                        data={
                            <PaymentDetail
                                session={ this.state.session }
                                pointTotal={ this.state.pointTotal }
                                pointSpent={ this.state.pointSpent }
                                pointCashbag={ this.state.pointCashbag }
                                couponData={ this.state.couponData }
                                couponCount={ this.state.couponCount }
                                totalCost={ this.state.totalCost }
                                specialDiscount = { this.state.specialDiscount }
                                totalDiscount = { this.state.totalDiscount }/>
                        } />
                </div>
                <PaymentExpection
                    payment = { this.state.payment }
                    mileage = { this.state.mileage }/>
            </div>
        )
    }
}

export default Container.create(PaymentViewer);
