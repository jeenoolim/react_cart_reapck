/***********************************************************
* @name			: {reactComponent} Terms.js
* @description		: 주류구매관련 정보수집 동의 
					: (장바구니에 주류상품이 선택 되어 있는 경우) 주문하기/결제하기 페이지 하단에 출력 
 * @import To 		: components/Footer.js
*************************************************************/
import React, { Component } from 'react'; //react 모듈
import PropTypes from 'prop-types'; //PropTypes 모듈 
import SlidePannel from '../ui/SlidePannel'; //SlidePannel 모듈 (열고 닫는 창)
import styles from './terms.css'; 


export default class Terms extends Component {
	
	 static propTypes = {
		data: PropTypes.any, //Footer.js 받아오는거 없는듯 
	}

	constructor(props){
		super(props);		
		this.state={
			open: false, // SlidePannel의 on/off 상태 
		}
		this.onClick = this.onClick.bind(this); // method binding 
	};

	onClick(e){
		// 클릭 하면,  this.state.open을 변경 한다.
		// setState 
		this.setState({ open: !this.state.open }); 
	}


	render() {
		  return (
			<div className={ styles.container }>
				<section className={ styles.terms_wrap }>
					<div className = "terms_header">
						<div onClick={ this.onClick }
							className={ `${styles.title} ${ this.state.open ? 'open' : 'close'}` } >전통주 구매자의 정보수집 및 이용에 동의합니다. 
						</div>
					</div>
					<div className="terms_description">
						<SlidePannel
							styles={ `${styles.adult_terms_description}`  }
							open={ this.state.open }
							data={
								 <div>
						<p>고객님께서는 아래 내용에 대하여 동의를 거부하실 수 있으며, 거부 시 상품배송, 구매 및 결제, 일부 포인트 적립이 제한됩니다.</p>
						<table>   
							<tbody>
								<tr>
									<th>수집·이용목적</th>
									<td>주류 통신판매기록부 관리 및 국세청 신고</td>
								</tr>
								<tr>
									<th>수집항목</th>
									<td>구매자의 이름, 주소, 생년월일 (본인인증 정보를 이용함)</td>
								</tr>
								<tr>
									<th>보유기간</th>
									<td>회원 탈퇴 시까지단,  관계법령의 규정에 따라 보존할 의무가 있으며 해당 기간 동안 보존</td>
								</tr>
							</tbody>
						</table>
						<p>이용계약(이용약관)이 존속중인 탈퇴하지 않은 회원의 경우 보유기간은 보존의무기간 이상 보관할 수 있으며 이 기간이 경과된 기록에 대해서 파기요청이 있는 경우 파기함</p>
								</div>
							}
						/>
					</div>
				</section>
		
		<section className={styles.adult_terms_agree}>
			<p>위 내용을 확인하였으며 결제에 동의 합니다.</p>
		</section>        
			</div>
		);
	}
}
