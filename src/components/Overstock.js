import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatLocal } from '../utils/UtilString';
import styles from './overstock.css';
import Item from '../ui/Item';

export default class Overstock extends Component {

    static propTypes = {
        data: PropTypes.object,
    };


    render() {
        let { data } = this.props;
        return (
            <div className={ styles.overstock }>
                <Item data={ data } styles={ styles }/>
                <div className={ styles.overlap }>
                    <div className={ styles.figuretitle }>{ data.name }</div>
                    <div className={ styles.figurecaption }>{`현재 재고는  `}
                        <span className={ styles.emphasis }><em>{ data.stock }</em></span>개 입니다.
                    </div>
                </div>
                <div className={ styles.count }>선택수량<span className={ styles.separate } /><em>{ data.count }</em>개</div>
            </div>
        )
    }
}


