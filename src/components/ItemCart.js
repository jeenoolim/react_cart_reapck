import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import ActionCreators from '../actions/ActionCreators';
import Item from '../ui/Item';
import Checkbox from '../ui/Checkbox';
import Button from '../ui/Button';
import styles from './itemCart.css';
import ItemController from './ItemController';
import ItemLabel from './ItemLabel';
import ItemCover from './ItemCover';
import ItemProps from './ItemProps';
import Alert from '../ui/Alert';
import Messages from '../dialog/Messages';
import Config from '../Config';

export default class ItemCart extends Component {
    static propTypes = {
        data: PropTypes.object,
        index: PropTypes.number,
        onChildUpdated: PropTypes.func,
        noStorageDumItemList: PropTypes.array,
    };

    /**
     * 생성자
     * @param  {[object]} props
     * @return {[void]}
     */
    constructor(props){
        super(props);
        this.onChecked = this.onChecked.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onIncrease = this.onIncrease.bind(this);
        this.onDecrease = this.onDecrease.bind(this);
        this.onDelete = this.onDelete.bind(this);
    };
    componentDidMount(){
        this.checkAndDeleteOfDumList();
    }
    /**
     * 컴포넌트 변경시
     * @param  {[object]} prevProps
     * @param  {[object]} prevState
     * @return {[void]}
     * 상위 컨테이너에서 선택 삭제대상을 수집하기위해 돔과 정보를 묶어 전달
     */
    componentDidUpdate(prevProps, prevState) {
        // const { index, data, onChildUpdated } = prevProps가 아니어도 되는 이유는, 
        // data.checked, data, .disabled, data.count는 모두 class의 getter기 때문에 최신상태를 나타내기 때문이다.(아마도)
        const { index, data, onChildUpdated } = this.props;
        onChildUpdated({
            index: index,
            checked: data.checked,
            disabled: data.disabled,
            node: ReactDOM.findDOMNode(this)
        });        
    }

    /**
     * 증정상품의 제고가 없는 상품인지 체크    
     */
    checkAndDeleteOfDumList(){        
        const { data, noStorageDumItemList } = this.props;
        let isDum = false;
        noStorageDumItemList.forEach(this.checkDumList.bind(this, data))
    }
    
    /**
     * 증정상품이 지정되어 있음에도 불구하고, 
     * 증정상품의 제고가 없는 상품이라면, 
     * 제거한다.
     */
    checkDumList(data, e, i, a){      
        if(e.parent_goodsno == data.code){
            alert(`${e.parent_goodsnm} 증정품의 재고수량이 0개 이므로 해당상품은 주문이 불가합니다.`)
            // 제거한다
            this.tween();
        }
    }
     
    /**
     * 체크/체크해제
     * @return {[void]}
     */
    onChecked(){
        const { data } = this.props;
        !this.overstock(data.count) && ActionCreators.updateItem(data, { checked: !data.checked });
    }

    /**
     * 상품갯수 변경
     * @return {[void]}
     */
    onIncrease(){
        const { data } = this.props;
        !this.overstock(data.next) && ActionCreators.updateItem(data, { count: data.next });
    }

    /**
     * 상품갯수 변경
     * @return {[void]}
     */
    onDecrease(){
        const { data } = this.props;
        if(data.count > 1){
            !this.overstock(data.prev) && ActionCreators.updateItem(data, { count: data.prev });
        }
    }

    /**
     * 상품 갯수 입력시
     * @param  {[number]} value
     * @return {[void]}
     * 서버에 변경 요청 하지 않고 저장소만 변경
     */
    onChange(value){
        const { data } = this.props;
        !this.overstock(value) && ActionCreators.updateItem(data, { count: value }, false);
    }

    /**
     * 포커스 해제시
     * @param  {[number]}
     * @return {[void]}
     * 서버와 저장소에 변경 요청
     */
    onBlur(value){
        const { data } = this.props;
        !this.overstock(value) && ActionCreators.updateItem(data, { count: value });
    }

    /**
     * 삭제버튼 클릭
     * @param  {[object]} 
     * @return {[void]}
     * 대화창 생성후 확인누르면 삭제 애니메이션
     */
    onDelete(e){
        ActionCreators.openDialog(<Alert message={ Messages.DELETE_SELECT_ITEM }
            onConfirm={ () => this.tween() }/>);
    }

    /**
     * 재고수량 초과시 재고 수량에 맞춤
     * @param  {number} next
     * @return {[void]}
     */
    
    overstock(next){
        const { data } = this.props;
        if(data.soldout){
            ActionCreators.openDialog(<Alert message={ Messages.SOLDOUT } />);
            return true;
        }
      
        if(data.overstock || next > data.stock || next > data.max){
            var msg = next > data.max ? Messages.OVER_COUNT : Messages.OVER_STOCK;
            var ea = next > data.max ? data.max : data.stock;
            ActionCreators.openDialog(<Alert message={ msg }
                onConfirm={ () => ActionCreators.updateItem(data, { count: ea }) }/>);
            return true;
        }

        
        return false;
    }


    /**
     * 상품 삭제 애니메이션
     * @return {[void]}
     * 대상 컨테이너는 다음 렌더링시 사용 하기 때문에 애니메이션 완료후 초기화 시켜줌.
     */
    tween(){
        const dom = ReactDOM.findDOMNode(this);
        TweenMax.to(dom, 0.3, { x: '110%', ease: Expo.easeInOut , onComplete: (tween) => {
            TweenMax.set(dom, { x: '0%' })
            ActionCreators.deleteItem(this.props.data);
        }})
    }

    /**
     * 렌더링
     * @return {[void]}
     */
    render() {
        let { data } = this.props;
        return (
            <div className={ `${styles.rootContainer} ${ !this.props.index && styles.first || '' }` }>
                <div className={ `${styles.container} ` }>
                {/* <ItemProps data={ data } />*/}
                    <div className={ styles.left }>
                        <Checkbox
                            styles={ `${styles.checkbox} ${ data.checked && styles.checked || '' }` }
                            checked={ data.checked }
                            onChecked={ this.onChecked }/>
                    </div>
                    <div className={ styles.center }>
                        <a href={ `${Config.ITEM_LINK_PATH}${data.code}` }>
                            <Item data={ data }/>
                        </a>
                        <ItemCover data={ data } />
                    </div>
                    <div className={ styles.right }>
                        <Button styles={ styles.deleteButton }
                            onPressTween={{ scale: 0.9 }}
                            onReleaseTween={{ scale: 1 }}
                            onClick={ this.onDelete }/>
                    </div>
                    <div className={ styles.bottom }>
                    <ItemController
                            data={ data }
                            onBlur={ this.onBlur }
                            onChange={ this.onChange }
                            onIncrease={ this.onIncrease }
                            onDecrease={ this.onDecrease }/>
                    </div>
                    <div className={ styles.labels }>
                    <ItemLabel data={ data }/>
                    </div>
                </div>
                <div className={ `${data.individual ? styles.hashTagForIndividualDelivery : styles.hide}` }>
                    <p>{`${Config.HASH_TAG_TEXT_FOR_INDIVIDUAL_DELIVERY}`}</p>
                </div>
            </div>
        );
    }
}
