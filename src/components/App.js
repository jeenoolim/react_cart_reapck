import React, { Component } from 'react';
import ReatDOM from 'react-dom';
import equal from 'deep-equal';
import { Container } from 'flux/utils';
import PropTypes from 'prop-types';
import ActionCreators from '../actions/ActionCreators';
import AppDispatcher from '../actions/AppDispatcher';
import Actions from '../actions/Actions';
import AppStore from '../stores/AppStore';
import ItemStore from '../stores/ItemStore';
import Footer from './Footer';
import Popup from '../ui/Popup';
import Dialog from '../ui/Dialog';
import Alert from '../ui/Alert';
import Notice from '../ui/Notice';
import Messages from '../dialog/Messages';
import ItemViewer from './ItemViewer';
import PaymentViewer from './PaymentViewer';
import CouponViewer from './CouponViewer';
import CashbagViewer from './CashbagViewer';
import OverstockViewer from './OverstockViewer';
import PaymentExpection from './PaymentExpection';


class App extends Component {

    /**
     * 앱 저장소
     * @return {[void]}
     */
    static getStores(){
        return [AppStore, ItemStore];
    }

    /**
     * 앱저장소 상태
     * @param  {[object]} prevState
     * @return {[void]}
     * mode: 모듈 실행 타입
     */
    static calculateState(prevState){
        return {
            mode: AppStore.mode,
            items: ItemStore.items,
            isFirstBuyer: AppStore.isFirstBuyer,
            cost: ItemStore.cost,
            includeFirstBuyerItem: ItemStore.includeFirstBuyerItem,
            firstBuyerItemFprodtno : ItemStore.firstBuyerItemFprodtno,
            firstBuyerItem: ItemStore.firstBuyerItem,
            session: AppStore.session,
            checkeds: ItemStore.checkeds,
            checkedItemsCode: ItemStore.checkedItemsCode,
            soldouts: ItemStore.soldouts,
            adults: ItemStore.adults,
            // optionable: ItemStore.optionable,
            // specific: ItemStore.specific,
            reservation: ItemStore.reservation,
            overstocks: ItemStore.overstocks,
            dawnOnlyItemsName: ItemStore.dawnOnlyItemsName,  
            // noReservations: ItemStore.noReservations,
            // onlyReservations: ItemStore.onlyReservations,
        }
    }

    /**
     * 렌더링 필터
     * @param  {[object]} nextProps
     * @param  {[object]} nextState
     * @return {[void]}
     */
    shouldComponentUpdate(nextProps, nextState) {
        const stateEqual = equal(this.state, nextState);
        return !stateEqual;
    }

    /**
     * 컴포넌트 마운트 시
     * @return {[void]}
     * 로그인 세션, 카트데이터, 쿠폰데이터, 배송정책 요청
     */
    componentDidMount(){
        this.registEvent();
        ActionCreators.requestSession();
        AppDispatcher.register(this.onReceived.bind(this));
    }

     /**
     * 쿠폰, 헬로패스등을 위한 순차적 데이터 로딩
     * @return {[void]}
     */
    onReceived(e){
        switch(e.type){
            case Actions.RECEIVED_SESSION : 
                ActionCreators.requestShippingPolicy();
                break;
            case Actions.RECEIVED_SHIPPING_POLICY :
                ActionCreators.requestCoupons();
                break;
            case Actions.RECEIVED_COUPONS: 
                ActionCreators.requestCartData();
                break;
            case Actions.RECEIVED_STOCKS:
                ActionCreators.closeDialog();
                if(!this.empty() && !this.overstocks()){             
                    window.submitCartForm(this.state.checkeds);
                }
                break;
            case Actions.RECEIVED_SYNC:
                window.submitCartForm(this.state.checkeds);
                break;

        }
    }

    /**
     * 서버 아이템변경시 이벤트 수신
     * @return {[void]}
     */
    registEvent(){
        const { mode } = this.state;
        if(mode === 'cart'){
            events.addListener('cart.data.changed', () => ActionCreators.requestCartData());
            events.addListener('cart.order.changed', event => ActionCreators.changeOrderCount(Number(event.data)));
            events.addListener('cart.data.submit', () => this.forward());
        }
    }


    /**
     * 주문상품이 없는지 검사
     * @return {[boolean]}
     */
    empty(){
        if(~~this.state.checkeds.length){
            return false;
        }
        ActionCreators.openDialog(<Alert message={ Messages.EMPTY_CART } onConfirm={ () => { ActionCreators.closeDialog() }} />);
        return true;
    }

    /**
     * 재고 수량을 초과한 상품이 없는지 검사
     * @return {[boolean]}
     */
    overstocks(){
        if(!~~this.state.overstocks.length){
            return false;
        }
        ActionCreators.openPopup(<OverstockViewer
            onConfirm={ () => {
                ActionCreators.requestSync();
            }}/>);
        return true;
    }

     /**
     * 예약배송 상품과 일반상품이 섞여 있는지 확인
     * @return {string} mixed, pure, none
     * ex) checker('able', 'only')
     */
    reservingChecker(type, name){
        const data = this.state.reservation[type],
            count = data[name].length,
            total = Object.keys(data).reduce((sum, key) => sum += data[key].length, 0);
            if(count > 0){
                return count < total && 'mixed' || 'pure';
            }
        return 'none';
    }
    
    forward(){
        const { mode, checkeds } = this.state;
        if(mode === 'cart'){

            if( this.state.isFirstBuyer && this.state.includeFirstBuyerItem ){
                //Fprodtno: 'n' : 100원 짜리 상품으로, 주문 합계금액 10,000원 이상일 경우에만 주문이 가능하다.
                if(this.state.firstBuyerItemFprodtno == 'n' && this.state.cost < 10000){
                    var firstBuyerItemName = this.state.firstBuyerItem ? this.state.firstBuyerItem.name : '';
                    alert('주문금액 1만원 미만인 경우 [첫구매전용]'+ firstBuyerItemName +'을 구매하실 수 없습니다.')
                    return false;
                //Fprodtno: 'y' : 9,800원 짜리 상품으로, 주문 합계금액 19,800원 이상일 경우에만 주문이 가능하다. 
                }else if (this.state.firstBuyerItemFprodtno == 'y' && this.state.cost < 19800){
                    var firstBuyerItemName = this.state.firstBuyerItem ? this.state.firstBuyerItem.name : '';
                    alert('이벤트 상품 외 주문금액 1만원 미만인 경우 [첫구매전용]'+ firstBuyerItemName +'을 구매하실 수 없습니다.')
                    return false;
                }
             
            }

            ActionCreators.requestStocks();
            return false;
        }

        window.submitOrderForm();
    }


   backward(){
        const { mode, reservation } = this.state;
        if(mode === 'cart'){
            window.openReservation(reservation.current.only);
            return false;
        }
        window.history.go(-1);
    }


    /**
     * 렌더링
     * @return {[void]}
     */
    render() {
        return (
            <div>
                <Dialog />
				<Popup />
                <ItemViewer mode={ this.state.mode } />
                <h4>결제예상금액</h4>
                <PaymentViewer />
                <Footer mode={ this.state.mode }
                    items={ this.state.items }
                    checkedItemsCode={ this.state.checkedItemsCode }                    
                    adults={ this.state.adults }
                    checker = { this.reservingChecker.bind(this) }
                    forward={ this.forward.bind(this) }
                    backward={ this.backward.bind(this) }/>
            </div>
        )
    }
}

export default Container.create(App);
