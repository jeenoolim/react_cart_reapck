import React, { Component } from 'react';
import { formatNumber, removeSpecipic } from '../utils/UtilString';
import PropTypes from 'prop-types';
import styles from './itemController.css';
import Button from '../ui/Button';

export default class ItemController extends Component {
    static propTypes = {
        data: PropTypes.object,
        onBlur: PropTypes.func,
        onIncrease: PropTypes.func,
        onDecrease: PropTypes.func,
    };

    /**
     * 생성자
     * @param  {[object]} props
     * @return {[void]}
     */
    constructor(props){
        super(props);
        this.state = { count: 0 };
        this.onIncrease = this.onIncrease.bind(this);
        this.onDecrease = this.onDecrease.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
    };
    
    /**
     * 컴포넌트 마운트 시
     * @return {[void]}
     * 새로 고침시 간혹 ReceiveProps가 호출 안되는 현상이 발생. 값 세팅해줌
     */
    componentDidMount() {
        this.setState({ count: this.props.data.count });
    }

    /**
     * 컴포넌트 속성 전달시
     * @param  {[object]} nextProps
     * @return {[void]}
     * 업데이트 된 값으로 세팅
     */
    componentWillReceiveProps(nextProps) {
        this.setState({ count: nextProps.data.count });
    }

    /**
     * 상품 갯수 올림
     * @return {[void]}
     *  ActionCreators.changeItemCount -> 변경할 아이템, 변경할 아이템 갯수
     */
    onIncrease(){
        this.props.onIncrease();
    }

    /**
     * 상품 갯수 내림
     * @return {[void]}
     * ActionCreators.changeItemCount -> 변경할 아이템, 변경할 아이템 갯수
     */
    onDecrease(){
        this.props.onDecrease();
    }

    /**
     * 상품 갯수 입력 시
     * @return {[void]}
     */
    onFocus(e){
        this.setState({ count: '' });
    }

    /**
     * 상품 갯수 입력 해제 시
     * @return {[vpid]}
     *
     */
    onBlur(e){
        let { data } = this.props,
            value = removeSpecipic(e.target.value);
        // 입력값이 없으면 원래 값으로 되돌리고 있다면 숫자로 변환 후 변경 요청
        value = !~~value.length && data.count || value;
        value = Number(value) > 1 && value || 1; 
        this.props.onBlur(Number(value));
    }

    /**
     * 상품 갯수 변경
     * @param  {[object]} e 키이벤트
     * @return {[void]}
     */
    onChange(e){
        let { data } = this.props, value = formatNumber(e.target.value);
        if(value > data.stock){
            this.onBlur(e);
            return false;
        }
        this.setState({ count: value });
    }

    /**
     * 렌더링
     * @return {[void]}
     */
    render() {
        let { count } = this.state;
        return (
            <div className={ styles.container }>
                <Button styles={ styles.minusButton }
                    onClick={ this.onDecrease } />
                <input className={ styles.count }
                    type="tel"
                    value={ removeSpecipic(count) }
                    onFocus={ this.onFocus }
                    onBlur={ this.onBlur }
                    onChange={ this.onChange }/>
                <Button styles={ styles.plusButton }
                    onClick={ this.onIncrease }/>
            </div>
        );
    }
}
