import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './itemLabel.css';

export default class ItemLabel extends Component {
    static propTypes = {
        data: PropTypes.object,
    };


    render() {
        let { data } = this.props;
        return (
            <div className={ styles.container } >
                <div className={ `${styles.label} ${!data.delay && styles.hide || ''}` }>지연배송</div>
                <div className={ `${styles.label} ${!data.individual && styles.hide || ''}` }>개별배송</div>
                <div className={ `${styles.label} ${styles.noReservation} ${data.reservation !== 'unable'  && styles.hide || ''}` }>예약배송불가</div>
                <div className={ `${styles.label} ${styles.onlyReservation} ${data.reservation !== 'only' && styles.hide || ''}` }>예약배송전용</div>
            </div>
        );
    }
}
