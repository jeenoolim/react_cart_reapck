import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './itemProps.css';

export default class ItemProps extends Component {
    static propTypes = {
        data: PropTypes.object,
    };

    constructor(props){
        super(props);
    }

    
    render() {
        const { data } = this.props;
        return (
            <div className={ styles.container }>
                <div className={ styles.prop }>상품번호 { data.code }</div>
                <div className={ styles.prop }>수량 { data.count }</div>
                <div className={ styles.prop }>재고 { data.stock }</div>
                <div className={ styles.prop }>상태 { data.status() }</div>
                <div className={ styles.prop }>배송 { data.specific }</div>
            </div>
        );
    }
}
