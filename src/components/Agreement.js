/***********************************************************
* @name			: {reactComponent} Terms.js
* @description		: 주류구매관련 정보수집 동의 
					: (장바구니에 주류상품이 선택 되어 있는 경우) 주문하기/결제하기 페이지 하단에 출력 
 * @import To 		: components/Footer.js
*************************************************************/
import React, { Component } from 'react'; //react 모듈
import PropTypes from 'prop-types'; //PropTypes 모듈 
import SlidePannel from '../ui/SlidePannel'; //SlidePannel 모듈 (열고 닫는 창)
import styles from './agreement.css'; 


export default class Agreement extends Component {
	
	static propTypes = {//reservingChecker
		items: PropTypes.array,
		mode: PropTypes.string	
	}

	constructor(props){
		super(props);		
		this.state={
            selected: -1
		}
		this.onClick = this.onClick.bind(this); // method binding 
	};


	onClick(index){
		// 클릭 하면,  this.state.open을 변경 한다.
        // setState 
		this.setState({ selected: this.state.selected == index ? - 1: index }); 
	}


	render() {
		const { selected } = this.state;
		const { mode } = this.props;
		const individuals = this.props.items.filter(item => item.checked && item.individual);
		return (
			<div className={ styles.container }
				style={{ display: mode == 'order' ? 'block' : 'none' }}>
				<section className={ styles.terms_wrap }>
					<div className = "terms_header">
						<div onClick={ () => { this.onClick(1) } }
							className={ `${styles.title} ${ selected == 1 ? 'open' : 'close'}` } >개인정보 수집및 이용동의
						</div>
					</div>
					<div className="terms_description">
						<SlidePannel
							styles={ `${styles.terms_description}`  }
							open={ selected == 1 }
							data={
								 <div className={ styles.terms_contents }>
                                    <p>고객님께서는 아래 내용에 대하여 동의를 거부하실 수 있으며, 거부 시 상품배송, 구매 및 결제, 일부 포인트 적립이 제한됩니다.</p>
                                    <table>   
                                        <tbody>
                                            <tr>
                                                <th>수집·이용목적</th>
                                                <td>대금 결제/환불 서비스 제공, 주문/배송/거래 내역 조회 서비스 제공, 전자상거래법 준수 등</td>
                                            </tr>
                                            <tr>
                                                <th>수집항목</th>
                                                <td>신용카드 정보, 계좌 정보, 주문/배송/거래 내역</td>
                                            </tr>
                                            <tr>
                                                <th>보유기간</th>
                                                <td>회원탈퇴 시 까지. 단, 관계 법령의 규정에 따라 보존할 의무가 있으면 해당 기간 동안 보존 </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>이용계약(이용약관)이 존속중인 탈퇴하지 않은 회원의 경우 보유기간은 보존의무기간 이상 보관할 수 있으며 이 기간이 경과된 기록에 대해서 파기요청이 있는 경우 파기함</p>
                                    <p>결제수단에 따른 개인정보 수집 및 이용 항목이 상이할 수 있음</p>
                                </div>
							}
						/>
					</div>
				</section>
				<section className={ styles.terms_wrap }
					style={{ display: individuals.length > 0 ? 'block' : 'none' }}>
					<div className = "terms_header" >
						<div onClick={ () => { this.onClick(2) } }
							className={ `${styles.title} ${ selected == 2 ? 'open' : 'close'}` } >개인정보 판매자 제공에 대한동의
						</div>
					</div>
					<div className="terms_description">
						<SlidePannel
							styles={ `${styles.terms_description}`  }
							open={ selected == 2 }
							height={ 180 }
							data={
								<div>
									<p>개인정보를 제공받는 자: { individuals.reduce((sum,item) => sum + `${item.maker}, `, '') }</p>
									<p>개인정보를 제공받는 자의 개인정보 이용 목적: 주문상품의 배송(예약)</p>
									<p>제공하는 개인정보의 항목: 성명, 주소, 연락처</p>
									<p>개인정보를 제공받는 자의 개인정보 보유 및 이용기간: 수취확인 후 3개월까지</p>
									<p>제공정보는 주문처리 및 배송을 위한 목적으로만 사용됩니다. 고객님께서는 위 내용에 대하여 동의를 거부하실 수 있으며, 거부시 상품 배송이 제한됩니다.</p>
								</div>
							}
						/>
					</div>
				</section>
                <section className={styles.terms_agree}>
                    <p>위 내용을 확인하였으며 결제에 동의 합니다.</p>
                </section>        
			</div>
		);
	}
}
