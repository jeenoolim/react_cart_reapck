import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './components/App';
import ActionCreators from './actions/ActionCreators';
// import 'babel-polyfill';
/**
 * script 태그에 삽입된 커스텀속성인 mode속성으로 장바구니와 주문페이지에 각각 삽입된 모듈을 구분함.
 */
const root = document.getElementById('cartItemViewer'),
    mode = root.getAttribute('mode'),
    direct = root.getAttribute('direct');
const render = (Component) => {
    ActionCreators.initialize(mode, direct);
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer>, root
    );
};
render(App);

module.hot.accept();



