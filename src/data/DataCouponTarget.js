import ItemStore from '../stores/ItemStore';

export default class DataCouponTarget{

    constructor(props) {
        this.props = props;
    }

    /**
     * 상품이름
     * @return {[string]}
     */
    get name(){
        return this.props.goodsnm;
    }

    /**
     * 상품 고유번호
     * @return {[string]}
     */
    get code(){
        return this.props.code;
    }

    /**
     * 상품 전시여부
     * @return {[boolean]}
     */
    get open(){
        return this.props.open === 'Y';
    }

    /**
     * 상품 품절여부
     * @return {[boolean]}
     */
    get soldout(){
        return this.props.soldout === 'Y';
    }

    /**
     * 장바구니에서 상품의 존재여부 반환
     * @return {[boolean]}
     */
    get active(){
        return ItemStore.contains('code', this.code, true);
    }

}