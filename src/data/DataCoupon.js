import DataCouponTarget from './DataCouponTarget';
import ItemStore from '../stores/ItemStore';
import AppStore from '../stores/AppStore';

export default class DataCoupon{

    constructor(props) {
        this.props = {
            ...props,
            checked: false,
            disabled: false,
            targets: this.createTargets(props.goodsnm_array),
        }
    }

    /**
     * 날자 표기변경
     * @return {[string]}
     */
    date(str){
        return str.substr(0, 11).replace(/\-+/g, '.');
    }

    /**
     * 특정상품 할인 객체를 CouponItem 클래스에 담아 배열형태로 추출.
     * @return {[array]}
     */
    createTargets(list){
        list = { ...list };
        return Object.keys(list).map(key => {
            return new DataCouponTarget({
                code: key,
                ...JSON.parse(list[key])
            });
        });
    }

    /**
     * 특정상품 할인쿠폰 일때  장바구니에 존재하는지 검사.
     * @return {[boolean]}
     */
    contains(key, value){
        return this.targets.some(item => item[key] === value);
    }


    /**
     * 쿠폰가격 반환
     * @return {[number]}
     * 가격 또는 할인율, 적립률
     */
    get price(){
        return this.props.price;
    }

    /**
     * 쿠폰타입 반환
     * @return {[string]}
     */
    get type(){
        return this.props.ability;
    }

    /**
     * 쿠폰타입 반환 (특정 상품 및 카테고리만 해당하는지(1), 전체에 해당 하는지(0))
     * @return {[string]}
     */
    get goodstype(){
        return this.props.goodstype;
    }

    /**
     * 쿠폰라벨 반환
     * @return {[string]} 0: 할인쿠폰, 1: 적립쿠폰, 2: 배송쿠폰
     */
    get label(){
        switch(this.type){
            case '1': return '적립쿠폰';
            case '2': return '배송쿠폰';
        }
        return '할인쿠폰';
    }

    /**
     * 무료배송이 아닐경우 쿠폰가격을 라벨 이름으로 반환.
     * @return {[string]}
     */
    get name(){
        return this.free && '무료배송' || this.price;
    }


    /**
     * 무료배송 조건
     * @return {[boolean]}
     */
    get free(){
        return this.price === '0' && this.type === '2';
    }

    /**
     * 쿠폰 고유번호 반환
     * @return {[string]}
     */
    get code(){
        return this.props.couponcd;
    }


    /**
     * 쿠폰기한 반환
     * @return {[string]}
     */
    get period(){
        return this.props.priodtype === '1' && `발급 후 ${this.date(this.props.sdate)} 일` ||
             `${this.date(this.props.sdate)} ~ ${this.date(this.props.edate)}`;
    }

    /**
     * 쿠폰이름 반환
     * @return {[string]}
     */
    get explain(){
        return this.props.summa;
    }


    /**
     * 쿠폰사용 조건
     * @return {[type]} [description]
     */
    get terms(){
        return this.props.excPrice;
    }


     /**
     * 쿠폰체크 여부
     * @return {[boolean]}
     */
    get checked(){
        return this.props.checked;
    }

    /**
     * 쿠폰체크 설정
     * @param  {[bool]} act
     * @return {[void]}
     */
    set checked(act){
        this.props.checked = act;
    }


    /**
     * 쿠폰의 사용가능 여부
     * @return {[boolean]}
     */
    get disabled(){
        const { cost, checkeds, shippingFree }  = ItemStore,
            total = cost / AppStore.orders;

        if(!checkeds.length){
            return true;
        }
        //전체상품 가격이 최소 요구금액에 충족하지 못할때
        if(this.terms > total){
            return true;
        }

        //쿠폰종류가 무료배송 쿠폰이지만 이미 미료배송 조건을 충족했을때
        if(this.free && total >= shippingFree){
            return true;
        }

        //특정상품 리스트가 존재하고 장바구니에 특정상품이 없을때
        if(this.targets.length && !checkeds.some(item => this.contains('code', item.code))){
            return true;
        }

        return false;
    }


    /**
     * 쿠폰의 사용가능 여부설정
     * @param  {[boolean]} act
     * @return {[void]}
     */
    set disabled(act){
        this.props.disabled = act;
    }

    /**
     * 쿠폰의 특정상품 할인 목록
     * @return {[array]}
     * 품절, 미진열 상품을 제외하고 반환
     */
    get targets(){
        return this.props.targets.filter(item => item.open && !item.soldout);
    }


    get max(){
        return Number(this.props.coupon_max);
    }

}