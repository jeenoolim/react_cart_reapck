import React from 'react';
import Config from '../Config';
import ActionCreators from '../actions/ActionCreators';
import Alert from '../ui/Alert';
import Actions from '../actions/Actions';
import Messages from '../dialog/Messages';

export default class DataItem{

    constructor(props, index) {
        const ea = Number(props.ea);
        this.props = {
            ...props,
            index: index,
            checked: props.selected === 'Y',
            discount: props.memberdc + props.special_discount_amount
        };
    }

    /**
     * 상품판매 상태
     * @return {[string]}
     * [closed, delay, soldout, normal]
     */
    status(){
        //상품 진열상태
        if(!this.open){
            return 'closed'; //미진열
        }

        //실제 품절된 상태
        if(this.runout) {
            //재고를체크하고 품절됐지만 판매하게된다면 품절에서 예외처리
            if(this.usestock && this.buyrunout) {
                return 'delay'; //배송 지연
            }
            return 'soldout'; //품절
        }
        return 'normal'; //정상
    }


    /**
     * 개별배송체크
     * @return {[boolean]}
     * @note: 안쓰는듯 하다. (get individual을 사용하는듯 하다.)
     */
    individual(){
        return this.props.data.deli_type === '1';
    }

    /**
     * 증가 가능한 수량
     * @return {[number]}
     */
    get next(){
        return this.count + 1;
    }

    /**
     * 감소 가능한 수량
     * @return {[number]}
     */
    get prev(){
        return this.count > 1 && this.count - 1 || 1;
    }

    /**
     * 아이템 수량 제어불가 여부 반환
     * @return {[boolean]}
     */
    get disabled(){
        //품절이거나, 배송지연 이거나, 미진열 상품 일때
        return this.soldout || this.closed;
    }


    /**
     * 상품 번호
     * @return {[string]}
     */
    get code(){
        return this.props.goodsno;
    }

    /**
     * 상품 이름
     * @return {[string]}
     */
    get name(){
        return this.props.goodsnm_en;
    }

    /**
     * 상품 아이디
     * @return {[string]}
     */
    get id(){
        return this.props.UniqID;
    }

    /**
     * 상품 가격
     * @return {[number]}
     */
    get price(){
        return Number(this.props.price);
    }

    /**
     * 개별 적립금
     * @return {[number]}
     */
    get mileage(){
        return Number(this.props.reserve);
    }

    /**
     * 상품 진열 여부
     * @return {[type]}
     */
    get open(){
        return this.props.open === '1';
    }
    /**
     * 상품 수량
     * @return {[number]}
     */
    get count(){
        if(this.disabled){
            return 0;
        }
        return Number(this.props.ea);
    }

    /**
     * 상품수량 변경
     * @param  {[number]} n
     * @return {[void]}
     */
    set count(n){
        // 아이템 수량변경시 체크 자동설정
        this.props.checked = n > 0;
        // 토글 체크시 수량복원용 restore 설정
        this.props.ea = n;

        this.props.restore = n > 0 && n || 1;
    }

    /**
     * 백업용 수량 반환
     * @return {[number]}
     */
    get restore(){
        return this.props.restore;
    }

    /**
     * 백업용 수량 저장
     * @return {[void]}
     */
    set restore(n){
        this.props.restore = n;
    }

    /**
     * 상품 체크 여부
     * @return {[boolean]}
     */
    get checked(){
        return !this.disabled && this.props.checked || false;
    }

    /**
     * 상품 체크 설정
     * @param  {[bool]} act
     * @return {[void]}
     */
    set checked(act){
        // 체크 활성화할때는 비활성할때 저장한 수량이 0일경우 1개로 강제세팅 해줌
        // this.props.ea = act && this.restore || 0;
        this.props.checked = act;
    }

    /**
     * 상품 재고
     * @return {[number]}
     */
    get stock(){
        return Number(this.props.validstock);
    }

    /**
     * 상품 재고 설정
     * @param  {[number]} n
     * @return {[void]}
     */
    set stock(n){
        this.props.validstock = n;
    }

    /**
     * 상품 품절처리 예외여부
     * @return {[boolean]}
     */
    get buyrunout(){
        return this.props.buyrunout === '1';
    }

    /**
     * 상품 품절처리 예외설정
     * @param  {[string]} n
     * @return {[void]}
     */
    set buyrunout(n){
        this.props.buyrunout = n;
    }

    /**
     * 상품 품절 상태
     * @return {[boolean]} [description]
     */
    get runout(){
        return this.props.runout === '1';
    }

    /**
     * 상품 품절 상태설정
     * @param  {[string]} n
     * @return {[void]}
     */
    set runout(n){
        this.props.runout;
    }

    /**
     * 재고 체크여부
     * @return {[boolean]}
     */
    get usestock(){
        return this.props.usestock === 'o';
    }

    /**
     * 재고 체크여부 설정
     * @param  {[string]} n
     * @return {[void]}
     */
    set usestock(n){
        this.props.usestock = n
    }

    /**
     * 재고 수량초과 여부
     * 지연배송 또는 개별배송 상품 일 경우 구매수량 초가 아님
     * @return {[boolean]}
     */
    get overstock(){
        if(this.delay || this.individual){
            return false;
        }
        return this.count > this.stock;
    }


    /**
     * 상품 이미지 경로
     * @return {[string]}
     */
    get image(){
        let path = $(this.props.cart_img).attr('src'),
            pattern = /^(https?:\/\/)?/g;
        return path.match(pattern)[0] === "" ? path.replace('../', '/shop/') : path;
    }

    /**
     * 상품 에러이미지
     * @return {[string]}
     */
    get errorImage(){
        return '/shop/data/skin/renew_C/img/common/noimg_130.gif';
    }

    /**
     * 상품 페이지 경로
     * @return {[string]}
     */
    get link(){
        return Config.ITEM_LINK_PATH + this.props.goodsno;
    }

    /**
     * 개별배송 여부
     * @return {[boolean]}
     */
    get individual(){
        return this.props.deli_type !== '1';
    }

    /**
     * 예약 배송정보
     * @return {[type]} [description]
     */
    get reservation(){
        if(this.props.unable_regular === 1){
            return 'unable';
        }
        if(this.props.regular_only === 1){
            return 'only';
        };
        return 'normal';
    }
    
        /**
         * 새벽배송 전용 상품 체크(있는지 없는지)
         * @return {[type]} [description]
         */
        get dawnOnly(){
          if(this.name.indexOf('새벽배송한정') > 0){
                return true;
          }else{
              return false;
          }
        }
    
    
    
        /**
         * 예약 배송 여부
     * @return {[boolean]}
     */
    // get reservation(){
    //     return this.props.unable_regular === 0;
    // }

    /**
     * 예약 배송만 가능
     * @return {[boolean]}
     */
    // get reserved(){
    //     return this.props.regular_only === 1;
    // }

    /**
     * 예약배송 관련 상태
     * @return {[string]}
     */
    // get specific(){
    //     if(this.props.unable_regular){
    //         return 'noReservation';
    //     }
    //     if(this.props.regular_only){
    //         return 'onlyReservation';
    //     }
    //     return 'normal';
    // }

    /**
     * 품절상태 체크
     * @return {[boolean]}
     */
    get soldout(){
        return this.status() === 'soldout';
    }

    /**
     * 배송지연상태 체크
     * @return {[boolean]}
     */
    get delay(){
        return this.status() === 'delay';
    }

    /**
     * 진열 상태 체크
     * @return {[boolean]}
     */
    get closed(){
        return this.status() === 'closed';
    }

    /**
     * 성인인인증 여부 반환
     * @return {[boolean]}
     */
    get adult(){
        return this.props.use_only_adult === '1';
    }

    /**
     * 특별할인
     * @return {[number]}
     */
    get discount(){
        return this.props.discount;
    }

    /**
     * 생산자 정보
     * @return {[string]}
     */
    get maker(){
        return this.props.maker;
    }

    /**
     * 아이템 원시객체
     * @return {[object]}
     */
    get data(){
        return this.props;
    }

    /**
     * 장바구니에서의 상품순서  반환
     * @return {[number]}
     */
    get index(){
        return this.props.index;
    }

    /**
     * 최대 구매 수량
     * @return {[number]}
     */
    get max(){
        const max = Number(this.props.max_ea);
        // 구매제한이 없을경우 수량이 0으로 들어옴.
        if(max < 1){
            return Number.MAX_SAFE_INTEGER;
        }
        return max;
    }



    /**
     * 장바구니에서 상품순서 세팅
     * @return {[number]}
     */
    set index(index){
        this.props.index = index;
    }

    /**
     * 첫구매 전용상품인지 확인
     * @return {[boolean]}
     */
    get isFirstBuyerItem(){
        if(this.props.fprodno){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 첫구매 전용상품 타입 확인
     * 'y' : 9,800원 짜리 상품으로, 주문 합계금액 19,800원 이상일 경우에만 주문이 가능하다. 
     * 'n' : 100원 짜리 상품으로, 주문 합계금액 10,000원 이상일 경우에만 주문이 가능하다. 
     * @return {[boolean]}
     */
    get fprodtno(){
        if(this.props.fprodtno){
            return this.props.fprodtno;
        }else{
            return false;
        }
    }

}
