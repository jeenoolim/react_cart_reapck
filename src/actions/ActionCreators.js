import AppDispatcher from './AppDispatcher';
import Actions from './Actions';
import request from '../api/request';
import helper from '../utils/common'

export default {

    /**
     * 장바구니 정보 불러오기
     * @return {[void]}
     */
    requestCartData(){
        request.requestCartData();
        // dispatch가 불필요 한것으로 보임(존재하는 store중 reduce가 REQUEST_CART_DATA를 받는 경우가 없어 보임 )
        AppDispatcher.dispatch({
            type: Actions.REQUEST_CART_DATA,
        })
    },
    /**
     * 장바구니 정보 불러오기 성공
     * @param  {[object]} data
     * @return {[void]}
     */
    receivedCartData(data){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_CART_DATA,
            data: data,
        })

        // 예약전용상품과, 예약배송 불가상품, 일반상품이 섞여 있는지 확인
        if(helper.isMixingReservationType()){
            helper.seperateReservationOnlyAndReservationUnableAndNormal({mode: "receivedCartData", item: data.item})
            return false
        }
   
        // order.html페이지에서 개별 배송상품만 있는지 확인
        helper.checkIndividualDeliveryAtIOrderPage()
        this.requestShippingCost();
    },

    /**
     * 장바구니 정보 불러오기 실패
     * @param  {[string]} err
     * @return {[void]}
     */
    requestCartDataError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_CART_DATA_ERROR,
            error: err,
        })
    },

    /**
     * 쿠폰정보 불러오기
     * @return {[void]}
     */
    requestCoupons(){
        request.requestCoupons();
        AppDispatcher.dispatch({
            type: Actions.REQUEST_COUPONS,
        })
    },

    /**
     * 쿠폰정보 불러오기 성공
     * @param  {[object]} data
     * @return {[void]}
     */
    receivedCoupons(data){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_COUPONS,
            data: data,
        })
        // this.requestShippingCost();
    },

    /**
     * 쿠폰 정보 불러오기 실패
     * @param  {[string]} err
     * @return {[void]}
     */
    requestCouponsError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_COUPONS_ERROR,
            error: err,
        })
    },


    /**
     * 배송비 정책 정보 불러오기
     * @return {[void]}
     */
    requestShippingPolicy(){
        request.requestShippingPolicy();
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SHIPPING_POLICY,
        })
    },

    /**
     * 배송비 정책 정보 불러오기 성공
     * @param  {[object]} data
     * @return {[void]}
     */
    receivedShippingPolicy(data){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_SHIPPING_POLICY,
            data: data,
        })
    },

    /**
     * 배송비 정책 정보 불러오기 실패
     * @param  {[string]} err
     * @return {[void]}
     */
    requestShippingPolicyError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SHIPPING_POLICY_ERROR,
            error: err,
        })
    },

    /**
     * 사용자 정보 불러오기
     * @return {[void]}
     */
    requestSession(){
        request.requestSession();
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SESSION,
        })
    },

    /**
     * 사용자 정보 불러오기 성공
     * @param  {[object]} data
     * @return {[void]}
     */
    receivedSession(data){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_SESSION,
            data: data,
        })
    },

    /**
     * 사용자 정보 불러오기 실패
     * @param  {[string]} err
     * @return {[void]}
     */
    requestSessionError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SESSION_ERROR,
            error: err,
        })
    },


    /**
     * 배송비 요청
     * @return {[void]}
     */
    requestShippingCost(){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SHIPPING_COST,
        })
        request.requestShippingCost();
    },
    /**
     * 배송비 수신
     * @param  {[string]} res
     * @return {[void]}
     */
    receivedShippingCost(res){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_SHIPPING_COST,
            cost: res,
        })
    },

    /**
     * 배송비 수신 에러
     * @param  {[string]} err
     * @return {[void]}
     */
    requestShippingCostError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SHIPPING_COST_ERROR,
            error: err,
        })
    },


    /**
     * 오케이 캐쉬백 카드번호 요청
     * @return {[void]}
     */
    requestCashbagCardNumber(){
        request.requestCashbagCardNumber();
        AppDispatcher.dispatch({
            type: Actions.REQUEST_CASHBAG_CARD_NUMBER,
        })
    },


    /**
     * 오케이 캐쉬백 카드번호 반환
     * @return {[void]}
     */
    receivedCashbagCardNumber(number){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_CASHBAG_CARD_NUMBER,
            number: number,
        })
    },

    /**
     * 오케이 캐쉬백 카드번호 반환 에러
     * @err {[string]}
     * @return {[void]}
     */
    requestCashbagCardNumberError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_CASHBAG_CARD_NUMBER_ERRPR,
            res: err,
        })
    },

    /**
     * 오케이 캐쉬백 포인트 조회
     * @param  {[string]} cardnumber
     * @param  {[string]} password
     * @return {[void]}
     */
    requestCashbagPoint(cardnumber, password){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_CASHBAG_POINT,
            cardnumber: cardnumber,
            password: password,
        })
        request.requestCashbagPoint(cardnumber, password);
    },

    /**
     * 오케이 캐쉬백 포인트, 요청아이디 반환
     * @param  {[string]} point
     * @param  {[string]} requestId
     * @return {[void]}
     */
    receivedCashbagPoint(point, requestId){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_CASHBAG_POINT,
            point: point,
            requestId: requestId,
        })
    },

    /**
     * 오케이 캐쉬백 포인트 에러
     * @param  {[string]} err
     * @return {[void]}
     */
    requestCashbagPointError(err, requestId){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_CASHBAG_POINT_ERROR,
            error: err,
            requestId: requestId,
        })
    },

    /**
     * 저장소 아이템 변경
     * @param  {[array]} items 변경 할 아이템 목록
     * @param  {[array]} values 변결 할 아이템의 수량 목록
     * @param  {[string]} name 변경할 속성 이름
     * @return {[void]}
     */
    updateItem(items, values, req = true){
        items = Array.isArray(items) && items || [items];
        values = Array.isArray(values) && values || [values];
        AppDispatcher.dispatch({
            type: Actions.UPDATE_ITEM,
            items: items,
            values: values,
        })
        req && request.requestItemUpdate(items, values);
    },


     /**
     * 서버 아이템수량 변경완료
     * @param  {[array]}  items 수량 변경완료 된 아이템목록
     * @param  {[string]} stock 현재 남아 있는 재고량
     * @return {[void]}
     */
    receivedItemUpdate(res, items, values){
        // dispatch가 불필요 한것으로 보임(존재하는 store중 reduce가 RECEIVED_ITEM_UPDATE 받는 경우가 없어 보임 )
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_ITEM_UPDATE,
            res: res,
        })
        // updateItem() 을 통하여 수량변경 한 것인지 확인 
        if(helper.isChangeCountAndMorethanOne(values)){
            helper.setChecked(items);
            return false;
        }

        // 예약전용상품과, 예약배송 불가상품, 일반상품이 섞여 있는지 확인
        if(helper.isMixingReservationType()){
            helper.seperateReservationOnlyAndReservationUnableAndNormal({mode: "updateItem", item: items})
            return false
        }

        this.requestShippingCost();
        
    },

    /**
     * 서버 아이템수량 변경요청 에러
     * @param  {[string]} err
     * @return {[void]}
     */
    requestItemUpdateError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_ITEM_UPDATE_ERROR,
            error: err,
        })
    },

    /**
     * 서버 아이템 삭제 요청
     * @param  {[object]} items
     * @return {[void]}
     */
    deleteItem(items){
        items = Array.isArray(items) && items || [items];
        AppDispatcher.dispatch({
            type: Actions.DELETE_ITEM,
            items: items,
        })
        request.requestItemDelete(items);
    },

     /**
     * 서버 아이템 삭제 수신
     * @param  {[object]} res
     * @return {[void]}
     */
    receivedItemDelete(res){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_ITEM_DELETE,
            res: res,
        })
        this.requestShippingCost();
    },

     /**
     * 서버 아이템 삭제에러 발생
     * @param  {[object]} err
     * @return {[void]}
     */
    requestItemDeleteError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_ITEM_DELETE_ERROR,
            error: err,
        })
    },

    /**
     * 전체 아이템 최대구매 수량으로 맞춤요청
     * @param  {Function} callback
     * @return {[void]}
     */
    requestSync(){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_SYNC
        })
        request.requestSync();
    },


    receivedSync(){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_SYNC,
        })
    },


    receivedSyncError(){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_SYNC_ERROR,
        })
    },

    /**
     * 전체 아이템 재고수량 요청
     * @param  {Function} callback
     * @return {[void]}
     */
    requestStocks(){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_STOCKS,
        })
        request.requestStocks();
    },

    /**
     * 전체 아이템 재고수량 수신
     * @return {[void]}
     */
    receivedStocks(codes, values){
        AppDispatcher.dispatch({
            type: Actions.RECEIVED_STOCKS,
            codes: codes,
            values: values,
        })
    },

    /**
     * 전체 아이템 재고수량 수신에러
     * @return {[void]}
     */
    requestStocksError(err){
        AppDispatcher.dispatch({
            type: Actions.REQUEST_STOCKS_ERROR,
            error: err,
        })
    },


     /**
     * 쿠폰 적용시
     * @param  {[number]} index
     * @return {[void]}
     */
    changeCoupon(index){
        AppDispatcher.dispatch({
            type: Actions.CHANGE_COUPON,
            index: index,
        })
        this.requestShippingCost();
    },

    /**
     * 쿠폰의 특정 할인상품 보기
     * @param  {[number]} index
     * @return {[void]}
     */
    unfoldCoupon(index){
        AppDispatcher.dispatch({
            type: Actions.UNFOLD_COUPON,
            index: index,
        })
    },

    /**
     * setTimeout, clearTimeout을 위한 객체
     * 예) 포인트 입력시, 포인트를 입력 할때마다 배송비를 요청하는 것은 불필요 하므로, 
     * 포인트 입력후 0.6초가 지나면 requestShippingCost를 호출한다.
     */
    timeoutToggle:{
        timeoutVariable: null,
        /**
         * timeoutToggle실행
         * @param {function} callback - 필수 
         * @param {*} time - 시간 ms
         */
        init(callback,time=600){
            if(!callback){
                return false;
            }
            this._clearTimeout();
            this._setTimeout(callback, time);
        },
        _clearTimeout(){
            clearTimeout(this.timeoutVariable)
        },
        _setTimeout(callback, time){
            this.timeoutVariable = setTimeout(function(){
                callback()
            }, time)
        },
    },

    /**
     * 적립된 마일리지 포인트 입력시
     * @param  {[string]} point
     * @return {[void]}
     */
    changePoint(point){
        AppDispatcher.dispatch({
            type: Actions.CHANGE_POINT,
            point: point,
        })
        this.timeoutToggle.init(this.requestShippingCost)
    },

    /**
     * 오케이 캐쉬백 카드번호 입력시
     * @param  {[string]} number
     * @return {[void]}
     */
    changeCashbagCardNumber(number){
        AppDispatcher.dispatch({
            type: Actions.CHANGE_CASHBAG_CARD_NUMBER,
            number: number,
        })
    },

     /**
     * 오케이 캐쉬백 카드비밀번호 입력시
     * @param  {[string]} password
     * @return {[void]}
     */
    changeCashbagPassword(password){
        AppDispatcher.dispatch({
            type: Actions.CHANGE_CASHBAG_PASSWORD,
            password: password,
        })
    },


     /**
     * 오케이 캐쉬백 포인트 입력시
     * @param  {[string]} point
     * @return {[void]}
     */
    changeCashbagPoint(point){
        AppDispatcher.dispatch({
            type: Actions.CHANGE_CASHBAG_POINT,
            point: point,
        })
        this.timeoutToggle.init(this.requestShippingCost)
    },

     /**
     * 팝업 띄우기
     * @param  {[string]} name
     * @return {[void]}
     */
    openPopup(data, option){
        AppDispatcher.dispatch({
            type: Actions.OPEN_POPUP,
            data: data,
            // option: option,
        })
    },

    /**
     * 팝업 닫기
     * @return {[void]}
     */
    closePopup(option){
        AppDispatcher.dispatch({
            type: Actions.CLOSE_POPUP,
            // option: option,
        })
    },


     /**
     * 경고창 띄우기
     * @param  {[any]} data
     * @option {[object]} option
     * @return {[void]}
     */
    openDialog(data, option){
        AppDispatcher.dispatch({
            type: Actions.OPEN_DIALOG,
            data: data,
            // option: option,
        })
    },

    /**
     * 경고창 닫기
     * @option {[object]} option
     * @return {[void]}
     */
    closeDialog(option){
        AppDispatcher.dispatch({
            type: Actions.CLOSE_DIALOG,
            // option: option,
        })
    },

    /**
     * 배송회차 변경시
     * @param  {[number]} count
     * @return {[void]}
     */
    changeOrderCount(count){
        AppDispatcher.dispatch({
            type: Actions.CHANGE_ORDER_COUNT,
            count: count,
        })
        this.requestShippingCost();
    },

    /**
     * 선택된 상품주문
     * @return {[void]}
     */
    submitData(mode){
        request.submit(mode);
        // AppDispatcher.dispatch({
        //     type: Actions.SUBMIT_DATA,
        // })
    },

    /**
     * 앱 실행
     * @param  {[string]} mode
     * @return {[void]}
     */
    initialize(mode, direct = '0'){
        AppDispatcher.dispatch({
            type: Actions.INITIALIZE,
            mode: mode,
            direct: direct,
        })
    }

}

