export default {

    REQUEST_CART_DATA                       : 'request cart data',
    RECEIVED_CART_DATA                      : 'received cart data',
    REQUEST_CART_DATA_ERROR                 : 'request cart data error',

    REQUEST_COUPONS                         : 'request coupons',
    RECEIVED_COUPONS                        : 'received coupons',
    REQUEST_COUPONS_ERROR                   : 'request coupons error',

    REQUEST_SHIPPING_POLICY                 : 'request shipping policy',
    RECEIVED_SHIPPING_POLICY                : 'received shipping policy',
    REQUEST_SHIPPING_POLICY_ERROR           : 'request shipping policy',

    REQUEST_SESSION                         : 'request session',
    RECEIVED_SESSION                        : 'received session',
    REQUEST_SESSION_ERROR                   : 'request session error',

    REQUEST_STOCKS                          : 'request stocks',
    RECEIVED_STOCKS                         : 'received stocks',
    REQUEST_STOCKS_ERROR                    : 'request stocks error',

    REQUEST_SYNC                            : 'request sync',
    RECEIVED_SYNC                           : 'received sync',
    REQUEST_SYNC_ERROR                      : 'request sync error',

    UPDATE_ITEM                             : 'update item',
    REQUEST_ITEM_UPDATE                     : 'request item update',
    RECEIVED_ITEM_UPDATE                    : 'received item update',
    REQUEST_ITEM_UPDATE_ERROR               : 'request item update error',

    DELETE_ITEM                             : 'delete item',
    REQUEST_ITEM_DELETE                     : 'request item delete',
    RECEIVED_ITEM_DELETE                    : 'received item delete',
    REQUEST_ITEM_DELETE_ERROR               : 'request item delete error',

    REQUEST_SHIPPING_COST                   : 'request shipping cost',
    RECEIVED_SHIPPING_COST                  : 'received shipping cost',
    REQUEST_SHIPPING_COST_ERROR             : 'request shipping cost error',

    REQUEST_CASHBAG_CARD_NUMBER             : 'request cashbag card number',
    RECEIVED_CASHBAG_CARD_NUMBER            : 'received cashbag card number',
    REQUEST_CASHBAG_CARD_NUMBER_ERROR       : 'request cashbag card number error',

    REQUEST_CASHBAG_POINT                   : 'request cashbag point',
    RECEIVED_CASHBAG_POINT                  : 'received cashbag point',
    REQUEST_CASHBAG_POINT_ERROR             : 'request cashbag point error',

    TOGGLE_ITEM                             : 'toggle item',
    OPTMIZE_ITEM                            : 'optimize item',

    CHANGE_POINT                            : 'change point',
    CHANGE_COUPON                           : 'change coupon',
    UNFOLD_COUPON                           : 'unfold coupon',

    CHANGE_CASHBAG_CARD_NUMBER              : 'change cashbag card number',
    CHANGE_CASHBAG_POINT                    : 'change cashbag card point',
    CHANGE_CASHBAG_PASSWORD                 : 'change cashbag password',

    CHANGE_ORDER_COUNT                      : 'change order count',

    OPEN_POPUP                              : 'open popup',
    CLOSE_POPUP                             : 'close popup',

    OPEN_DIALOG                             : 'open dialog',
    CLOSE_DIALOG                            : 'close dialog',

    SUBMIT_DATA                             : 'submit data',
    INITIALIZE                              : 'initialize',
}