import update from 'react-addons-update';
import { ReduceStore } from 'flux/utils';
import { formatNumber } from '../utils/UtilString';
import common from '../utils/common';
import DataCoupon from '../data/DataCoupon';
import AppDispatcher from '../actions/AppDispatcher';
import Actions from '../actions/Actions';
import ItemStore from './ItemStore';
import AppStore from './AppStore';


class CouponStore extends ReduceStore {

    getInitialState(){
        return {
            coupons: [],
            selected: -1,
            unfolded: -1,
            benefits: {
                discount: 0,
                mileage: 0,
                shipping: 0
            }
        }
    }

    /**
     * 쿠폰 객체를 클래스 형태로 전환
     * @param  {[array]} [object items]
     * @return {[array]} [class Coupon]
     */
    create(coupons){
        if(coupons){         
            return coupons.map(coupon => new DataCoupon(coupon));
        }
        return [];
    }

    /**
     * 쿠폰혜택 업데이트
     * @return {[void]}
     */
    benefits(coupon){
        let discount,
            benefits = this.getState().benefits;
        benefits = Object.keys(benefits).reduce((benefit, key) => {
            benefit[key] = 0;
            return benefit;
        }, {});

        if(!coupon){
            return benefits;
        }

        return {
            ...benefits,
            discount: coupon.type === '0' && this.calculate(coupon, 'price'),
            mileage: coupon.type === '1' && this.calculate(coupon, 'mileage'),
            shipping: coupon.free && ItemStore.shippingBasic || 0,
        };
    }

    /**
     * 할인 또는 마일리지 계산
     * @param  {[object]} coupon
     * @param  {[string]} key
     * @return {[number]}
     */
    calculate(coupon, key){

        //쿠폰값
        let amount = coupon.price;
        //쿠폰값이 퍼센트 값이 아니면 값 반환
        if(!~amount.toString().indexOf('%')){
            // 쿠폰 타입이 'price'라면 2017.12.13 변경된 함수를 사용(계산된 값 반환)
            if(coupon.type === '0' && coupon.goodstype === '1'){
                let checkedCartitemInCoupon = ItemStore.checkeds.filter(item => coupon.contains('code', item.code))
                let amountOfCheckedCartitemInCoupon = checkedCartitemInCoupon.reduce((sum, item) => sum + Number(item[key]) * item.count , 0);
                // 장바구니에서 체크된 상품 중 '쿠폰에 해당하는 상품가격의 합'(맴버할인 및 특별할인이 적용되지 않은 판매가격)이 쿠폰의 액면가 보다 작으면, 
                // 장바구니에서 체크된 상품 중 '쿠폰에 해당하는 상품가격의 합'만큼 할인 해주고
                // 아니면, 쿠폰의 액면가 만큼 할인 해준다.
                let appliedAmountOfCoupon = amountOfCheckedCartitemInCoupon < coupon.price ? amountOfCheckedCartitemInCoupon : coupon.price
     
                return Number(appliedAmountOfCoupon);
            }else{ 
                return Number(amount);
            }
        }
  
        let result = (coupon.targets.length && ItemStore.checkeds.filter(item => coupon.contains('code', item.code)) || ItemStore.checkeds)
        .reduce((sum, item) => sum += item[key] * item.count * formatNumber(amount) * 0.01, 0);
        result = common.floor(result, -1);
        //특정상품할인, 가격할인, 마일리지 적립 판단, 합산하여 반환
        return result > coupon.max && coupon.max || result;
    }

    /**
     * 쿠폰 객체 리스트 반환
     * @return {[array]}
     */
    get coupons(){
        return this.getState().coupons;
    }

    /**
     * 쿠폰객체 리스트 갯수 반환
     * @return {[number]}
     */
    get count(){
        return this.getState().coupons.length;
    }

    /**
     * 선택된 쿠폰 인덱스 반환
     * @return {[number]}
     */
    get selected(){
        return  this.getState().selected;
    }

    /**
     * 쿠폰의 특정상품 할인 목록 펼침 여부
     * @return {[boolean]}
     */
    get unfolded(){
        return this.getState().unfolded;
    }

    /**
     * 선택된 쿠폰정보 반환
     * @return {[type]} [description]
     */
    get current(){
        return AppStore.toggle && this.coupons[this.selected] || null;
    }

    /**
     * 선택된 쿠폰의 배송비 혜택 반환
     * @return {[number]}
     */
    get shipping(){
        return this.getState().benefits.shipping;
    }

    /**
     * 선택된쿠폰의 가격할인 혜택 반환
     * @return {[number]}
     */
    get discount(){
        return AppStore.toggle && this.getState().benefits.discount || 0;
    }

    /**
     * 선택된쿠폰의 마일리지 적립 혜택 반환
     * @return {[number]}
     */
    get mileage(){
        return this.getState().benefits.mileage;
    }

    /**
     * 쿠키저장
     * @param  {[number]} index
     * @return {[void]}
     */
    setCookie(index){
        const coupon = this.coupons[index];
        window.cookies.create('cart.coupon', coupon === undefined && '0' || coupon.code, 1);
    }

     /**
     * 쿠키호출
     * @return {[number]}
     */
    getCookie(coupons){
        let code;
        (AppStore.mode === 'cart') && this.setCookie(-1);
        code = window.cookies.read('cart.coupon');
        return coupons.findIndex(coupon => coupon.code === code);
    }

    /**
     * 리듀서
     * @param  {[object]} state
     * @param  {[object]} action
     * @return {[void]}
     */
    reduce(state, action){
        switch(action.type){

            case Actions.RECEIVED_COUPONS:
                const coupons = this.create(action.data);
                return update(state, {
                    coupons: { $set: coupons },
                });
            case Actions.RECEIVED_CART_DATA:
                AppDispatcher.waitFor([ItemStore.getDispatchToken()]);
                const index = this.getCookie(state.coupons);
                return update(state, {
                    selected: { $set: index },
                    benefits: { $set: this.benefits(state.coupons[index]) },
                });
            case Actions.DELETE_ITEM:
            case Actions.UPDATE_ITEM:
            case Actions.CHANGE_COUPON:
                this.setCookie(action.index);
                return update(state, {
                    selected: { $set: action.index === undefined && -1 || action.index },
                    benefits: { $set: this.benefits(this.coupons[action.index]) },
                });
            case Actions.UNFOLD_COUPON:
                return update(state, {
                    unfolded: { $set: action.index == state.unfolded ? -1 :  action.index },
                });
            default :
                return state;
        }
    }
}

export default new CouponStore(AppDispatcher);
