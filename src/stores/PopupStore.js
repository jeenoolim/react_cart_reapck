import update from 'react-addons-update';
import { ReduceStore } from 'flux/utils';
import AppDispatcher from '../actions/AppDispatcher';
import Actions from '../actions/Actions';

class PopupStore extends ReduceStore {
    getInitialState(){
        return {
            active: false,
            contents: null,
            option: null,
        }
    }

    reduce(state, action){
        switch(action.type){
            case Actions.OPEN_POPUP:
                document.body.style.overflow = 'hidden';
                return update(state, {
                    contents: { $set: action.data },
                    option: { $set: action.option },
                    active: { $set: true },
                });
            case Actions.CLOSE_POPUP:
                document.body.removeAttribute('style');
                return update(state, {
                    active: { $set: false },
                });
            default : return state;
        }
    }

    get active(){
        return this.getState().active;
    }

    get contents(){
        return this.getState().contents;
    }

    get option(){
        return this.getState().option;
    }

    get scroll(){
        return this.getState().scroll;
    }
}

export default new PopupStore(AppDispatcher);
