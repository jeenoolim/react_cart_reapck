import update from 'react-addons-update';
import { ReduceStore } from 'flux/utils';
import * as Config from '../Config';
import DataItem from '../data/DataItem';
import Actions from '../actions/Actions';
import AppDispatcher from '../actions/AppDispatcher';
import AppStore from './AppStore';
import CouponStore from './CouponStore';
import CashbagStore from './CashbagStore';


class ItemStore extends ReduceStore {

    /**
     * 아이템 저장소 정보
     * @return {[void]}
     */
    getInitialState(){
        return {
            data: null,
            items: [],
            direct: 0,
            optimized: false,
            hellopass: 0,
            point: {
                total: 0,
                spent: 0
            },
            shipping: {
                tip: '',
                free: 0,
                basic: 0,
                cost: 0
            }
        }
    }


    /**
     * 아이템 목록 반환
     * @param  {Boolean} checked 선택된 아이템 반환 여부
     * @return {[array]}
     */
    list(checked){
        return checked && this.checkeds || this.items;
    }

    /**
     * 모든상품의 요청속성과 갯수의 합산하여 반환
     * @param  {[string]} prop
     * @return {[number]}
     */
    sum(key, checked = false){
        return this.list(checked).reduce((sum, item) => sum + Number(item[key]) * item.count , 0);
    }

    /**
     * 모든상품 중 요청속성과 값이 일치하는 상품이 포함되어 있는지 판단.
     * @param  {[string]} code
     * @return {[any]}
     * @param  {Boolean} checkds
     */
    contains(key, value, checked = false){
        return this.list(checked).some(item => item[key] === value);
    }

    /**
     * 모든상품 중 요청속성과 값이 일치하는 상품을 목록으로 반환
     * @param  {[string]} code
     * @return {[any]}
     * @param  {Boolean} checkds
     */
    filter(key, value, checked = false){
        return this.list(checked).filter(item => item[key] === value);
    }


    /**
     * 아이템 고유번호로 아이템 반환
     * @param  {[string]} code
     * @return {[object]}
     */
    findItem(code, list = this.items){
        return list.find(item => item.code === code);
    }

    /**
     * 아이템으로 아이템 인덱스 반환
     * @param  {[object]} item
     * @return {[number]}
     */
    findIndex(code, list = this.items){
        return list.findIndex(item => item.code === code );
    }

    /**
     * 아이템 정보변경
     * @param  {[array]} codes 아이템객체 아이디 목록
     * @param  {[array]} props 설정할 키와 값 목록
     * @return {[void]}
     * 아이템 아이디 리스트 와 변경할 속성만큼 순환하며 아이템 객체의 속성을 변경
     */
    updateItem(items, props){
        let index, prop, list = [...this.items];
        
        //아이템의 아이디 목록 수 만큼 순회하며
        items.map((item, i) => {
            //변경할 속성 리스트에서 속성 선택
            prop = props[i];
            //설정한 속성으로 모두 변경
            Object.keys(prop).map(key => { 
                //설정한 속성으로 모두 변경
                item[key] = prop[key] ;
            });
            //리스트에서 교체할 자리 선택
            index = this.findIndex(item.code);
            list = update(list, {
                [index]: { $set: item }
            });
        })
        return list;
    }

    

    /**
     * 아이템 삭제
     * @param  {[array]} items
     * @return {[void]}
     */
    deleteItem(items){
        let index,
            list = this.items;
        items.map((item, i) => {
            index = this.findIndex(item.code, list);
            list = update(list, { $splice: [[ index, 1]] });
        })
        list.map((item, i) => item.index = i );
        return list;
    }

    /**
     * 아이템 객체를 클래스 형태로 전환
     * @param  {[array]} [object items]
     * @return {[array]} [class items]
     */
    create(items){
        let list = [];
        items.map((item, index) => list.push(new DataItem(item, index)));
        return list;
    }



    /**
     * 사용자 누적 포인트 반환
     * @return {[number]}
     */
    get pointTotal(){
        return AppStore.toggle && this.getState().point.total || 0;
    }

    /**
     * 입력된 포인트 반환
     * @return {[number]}
     */
    get pointSpent(){
        return AppStore.toggle && Number(this.getState().point.spent) || 0;
    }

    /**
     * 상품 전체가격 반환
     * @return {[number]}
     */
    get cost(){
        return this.sum('price', true) * AppStore.orders;
    }

    /**
     * 기타 할인을 제외한 지불금액 반환
     * @return {[number]}
     * 상품가격, 배송비, 특별할인
     */
    get totalCost(){
        return this.cost + this.shippingCost - this.specialDiscount;
    }

    /**
     * 멤버 할인
     * @return {[number]}
     */
    get specialDiscount(){
        // sum 함수 안의 discount는 Dataitem.js의 (props.memberdc + props.special_discount_amount)을 의미하고, 
        // AppStore.orders는 AppStore.js에 따라 '예약배송 회차반환을' 의미한다. 
        return Number(this.sum('discount', true)) * AppStore.orders;
    }

    /**
     * 전체 할인 반환
     * @return {[number]}
     * 특별할인, 포인트할인, 쿠폰할인, 캐쉬백 할인
     */
    get totalDiscount(){
        return this.specialDiscount + this.pointSpent + CouponStore.discount + CashbagStore.spent;
    }

    /**
     * 지불 금액 반환
     * @return {[number]}
     * 상품가격, 전체할인, 배송비
     */
    get payment(){
        return this.cost - this.totalDiscount + this.shippingCost;
    }

    /**
     * 전체 상품목록 반환
     * @return {[array]}
     */
    get items(){
        return this.getState().items;
    }

    /**
     * 체크된상품 목록반환
     * @return {[array]}
     */
    get checkeds(){
        // this.items.filter(item => item[checked] === true)
        return this.filter('checked', true);
    }


    /**
     * checked된 아이탬의 상품번호를 배열로 반환한다.
     */
    get checkedItemsCode(){    
        let checkedItemsCode = []
        this.checkeds.forEach(function(e, i){
             checkedItemsCode.push(e.code)
         })        
         return checkedItemsCode
    }

    /**
     * 모든 아이탬의 상품번호를 배열로 반환한다.
     */
    get allItemsCode(){    
        let allItemsCode = []
        this.items.forEach(function(e, i){
            allItemsCode.push(e.code)
         })        
         return allItemsCode
    }

    /**
     * 품절, 미진열 상품목록 반환
     * @return {[array]}
     */
    get disableds(){
        return this.filter('disabled', true);
    }

    /**
     * 품절 상품이나 미판매 상품 리스트 반환
     * @return {[array]}
     */
    get soldouts(){
        return this.filter('disabled', true);
    }


    /**
     * 배송정책 설명 반환
     * @return {[string]}
     */
    get shippingTip(){
        return this.getState().shipping.tip;
    }

    /**
     * 기본 배송료
     * @return {[number]}
     */
    get shippingBasic(){
        return this.getState().shipping.basic;
    }

    /**
     * 배송비 반환
     * @return {[number]}
     */
    get shippingCost(){
        const cost = Number(this.getState().shipping.cost);
        return AppStore.toggle && cost || 0;
    }

    /**
     * 무료배송 최소 요구 금액
     * @return {[number]}
     */
    get shippingFree(){
        return Number(this.getState().shipping.free);
    }

    /**
     * 헬로패스 대상자 여부 반환
     * @return {[number]}
     */
    get hellopass(){
        return Number(this.getState().hellopass);
    }

    /**
     * 적립금 반환
     * @return {[number]}
     */
    get mileage(){
        return this.sum('mileage', true) + CouponStore.mileage;
    }

    /**
     * 장바구담기 또는 구매하기로 유입된 사용자 구분.
     * @return {[boolean]}
     * 0:장바구니로 유입, 1: 구매하기로 유입
     */
    get direct(){
        return this.getState().direct;
    }


     /**
     * 예약 배송 분류 목록
     * @param  {Boolean} checked
     * @return {[void]}
     */
    reservationTypes(options){

        if(options.ableGoods){
            return {
                only: this.ableGoods.filter(item => item['reservation'] === 'only'),
                unable: this.ableGoods.filter(item => item['reservation'] === 'unable'),
                normal: this.ableGoods.filter(item => item['reservation'] === 'normal'),
            }
        }
        return {
            only: this.filter('reservation', 'only', options.checked),
            unable: this.filter('reservation', 'unable', options.checked),
            normal: this.filter('reservation', 'normal', options.checked),
        }
    }


    
    /**
     * 장바구니에 포함된 상품의 예약배송 분류 리스트
     * @return {[type]} [description]
     */
    get reservation(){
        return {
            total: this.reservationTypes({ableGoods: false, checked: false}),
            current: this.reservationTypes({ableGoods: false, checked: true}),
            able:this.reservationTypes({ableGoods: true, checked: true})
        }
    }

    /**
     * 선택된 상품에 '예약배송 전용상품' 과 '예약배송 불가상품'중 어떤것이 있는지 확인
     *  @return {string} 
     *  - only: '예약배송 전용상품'만 선택되어 있음(일반상품 선택 여부는 상관 없음)
     *  - unable: '예약배송 불가상품'만 선택되어 있음(일반상품 선택 여보는 상관 없음)
     *  - mix: '전용상품'과 '불가상품'이 모두 포함되어 있음
     *  - normal : '일반상품만' 포함되어 있음 
     */
    get listReservationType(){
        if(this.reservation.current.only.length > 0 && this.reservation.current.unable.length < 1){
            return 'only'
        } else if (this.reservation.current.only.length < 1 && this.reservation.current.unable.length > 0){
            return 'unable'
        } else if ( this.reservation.current.only.length > 0 && this.reservation.current.unable.length > 0){
            return 'mix'
        } else {
            return 'normal'
        }
    }


    get listReservationAndNormal(){
        if(this.reservation.current.only.length > 0 && this.reservation.current.normal.length > 0){
            return 'mix'
        } else {
            return 'normal'
        }
    }

    /**
     * 개별배송 상품만 선택 되어 있는지 확인
     * @return {boolean}
     */
    get isOnlyIndividualDelivery(){
        return this.deliveryType.current.individual.length > 0 && this.deliveryType.current.normal.length <= 0
    }

    /**
     * 장바구니에 포함된 상품의 개별배송 분류 리스트
     * @return {array} 해당하는 상품 리스트 
     * @return {object} {total: {indiviual: [], normal: []} , current: {indiviual: [], normal: []}}  
     */
    get deliveryType(){
        return {
            total: this.checkDeliveryType(false),
            current: this.checkDeliveryType()
        }
    }

    /**
     * 장바구니 상품중 개별배송 상품들의 리스트와, 그렇지 않은 상품 리스트를 반환
     * @param {boolean} checked 
     * @return {object} {indiviual: [], normal: []} 
     */
    checkDeliveryType(checked = true){
        return {
            individual: this.filter('individual', true, checked),
            normal: this.filter('individual', false, checked),
        }    
    }

    /**
     * 장바구니에 포함된 상품에 새벽배송 전용 상품이 있는지 확인
     * @return {[type]} [description]
     */
    get dawnOnlyItemsName(){
        let dawnOnlyItems = this.filter('dawnOnly', true, true);
        let dawnOnlyItemsName = dawnOnlyItems.map(v => v.name)
        return dawnOnlyItemsName     
    }



    /**
     * 구매가능 상품 리스트 반환
     * @return {[array]}
     */
    get ableGoods(){
        return this.filter('disabled', false);
    }
    
    /**
     * 재고수량을 초과한 상품 리스트 반환
     * @return {[array]}
     */
    get overstocks(){
        return this.filter('overstock', true, true);
    }


     /**
     * 성인인증 상품 리스트 반환
     * @return {[array]}
     */
    get adults(){
        return this.filter('adult', true, true);
    }

    /**
     * 선택된 상품중에 첫구매 전용상품이 포함 되어 있는지 확인
     */
    get includeFirstBuyerItem(){
        return this.contains('isFirstBuyerItem', true, true)
    }

    /**
     * 선택된 상품중에 첫구매 전용상품 타입(fprodtno) 확인
     */
    get firstBuyerItemFprodtno(){
        if(this.firstBuyerItem){
            return this.firstBuyerItem.fprodtno
        }else{
            return false
        }
    }


    /**
     * 첫구매 상품이 있는 경우 해당 아이탬 리턴
     */
    get firstBuyerItem(){
        var items = this.checkeds.filter(function(e,i){
            return e.isFirstBuyerItem == true;
        })
        return items ? items[0]: [];
    }


    /**
     * 쿠키저장
     * @param  {[number]} index
     * @return {[void]}
     */
    setCookie(point){
        window.cookies.create('cart.point', point === undefined && 0 || point, 1);
    }

     /**
     * 쿠키호출
     * @return {[number]}
     */
    getCookie(mode){
        mode === 'cart' && this.setCookie(0);
        return window.cookies.read('cart.point');
    }



    /**
     * 리듀서
     * @param  {[opbject]} state
     * @param  {[string]}
     * @return {[object]}
     */
    reduce(state, action){
        switch(action.type){
            //상품 데이터 로드 완료
            case Actions.RECEIVED_CART_DATA:                
                return update(state, {
                    items: { $set: this.create(action.data.item) },
                    point: {
                        total: { $set: action.data.mypoint }
                    },
                    shipping: {
                        tip: { $set: action.data.deliMessageis }
                    },
                    direct: { $set: action.data.isDirect },
                });
            //기본 배송비와 무료배송 정보 저장
            case Actions.RECEIVED_SHIPPING_POLICY:
                return update(state, {
                    shipping: {
                        basic: { $set: action.data.default },
                        free: { $set: action.data.free }
                    },
                    hellopass: { $set: action.data.hellopass_gcode }
                });
                return state;
            // 전체 아이템 토글
            case Actions.TOGGLE_ITEM:
                return update(state, {
                    items: { $set: this.updateItem(state.items, Array(state.items.length).fill({ checked: action.toggle })) },
                })
            //변경할 아이템 목록, 변경하고자 하는 값, 변경할 속성 이름
            case Actions.UPDATE_ITEM:
                this.setCookie(0);
                return update(state, {
                    items: { $set: this.updateItem(action.items, action.values) },
                    point: { spent: { $set: 0 } }
                })
            // 아이템 삭제
            case Actions.DELETE_ITEM:
                this.setCookie(0);
                return update(state, {
                    items: { $set: this.deleteItem(action.items) },
                    point: { spent: { $set: 0 } }
                })
            case Actions.RECEIVED_ITEM_DELETE:
                window.setCartCount(this.items.length);
                return state;
            case Actions.CHANGE_POINT:
                this.setCookie(action.point);
                return update(state, {
                    point: { spent: { $set: action.point } }
                });
            // case Actions.RECEIVED_STOCKS:
            //     return update(state, {
            //         items: { $set: this.updateItem(action.codes.map(code => this.findItem(code)), action.values) },
            //     });
            case Actions.RECEIVED_SHIPPING_COST:
                return update(state, {
                    shipping: {
                        cost: { $set: action.cost },
                    }
                });
            case Actions.INITIALIZE:
                return update(state, {
                    point: { spent: { $set: this.getCookie(action.mode) } }
                });
            default:
                return state;
        }
    }


}

export default new ItemStore(AppDispatcher);
