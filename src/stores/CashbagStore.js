import update from 'react-addons-update';
import { ReduceStore } from 'flux/utils';
import { formatNumber } from '../utils/UtilString';
import DataCoupon from '../data/DataCoupon';
import AppDispatcher from '../actions/AppDispatcher';
import Actions from '../actions/Actions';
import AppStore from './AppStore';


class CashbagStore extends ReduceStore {

    getInitialState(){
        return {
            registed: false,
            number: '',
            password: '',
            requestId: null,
            error: null,
            total: 0,
            spent: 0,
        }
    }

    get registed(){
        return this.getState().registed;
    }

    get number(){
        return this.getState().number;
    }

    get password(){
        return this.getState().password;
    }

    get requestId(){
        return this.getState().requestId;
    }

    get error(){
        return this.getState().error;
    }

    get total(){
        return Number(this.getState().total);
    }

    get spent(){
        return AppStore.toggle && Number(this.getState().spent) || 0;
    }

     /**
     * 쿠키저장
     * @param  {[number]} index
     * @return {[void]}
     */
    setCookie(point){
        window.cookies.create('cart.cashbag', point === undefined ? 0 : `${point},${this.requestId}`, 1);
    }

     /**
     * 쿠키호출
     * @return {[number]}
     */
    getCookie(mode){
        let cookie;
        mode === 'cart' && this.setCookie(0);
        cookie = window.cookies.read('cart.cashbag');
        return cookie && cookie.split(',')[0] || 0;
    }

    /**
     * 리듀서
     * @param  {[object]} state
     * @param  {[object]} action
     * @return {[void]}
     */
    reduce(state, action){
        switch(action.type){
            case Actions.RECEIVED_CASHBAG_CARD_NUMBER:
                return update(state, {
                    number: { $set: action.number || ''   },
                });
            case Actions.RECEIVED_CASHBAG_POINT:
                return update(state, {
                    total: { $set: action.point },
                    requestId: { $set: action.requestId },
                    error: { $set: null }
                });
            case Actions.REQUEST_CASHBAG_POINT_ERROR:
                return update(state, {
                    requestId: { $set: action.requestId },
                    error: { $set: action.error },
                });
            case Actions.CHANGE_CASHBAG_CARD_NUMBER:
                return update(state, {
                    number: { $set: action.number },
                });
            case Actions.CHANGE_CASHBAG_PASSWORD:
                return update(state, {
                    password: { $set: action.password },
                });
            case Actions.DELETE_ITEM:
            case Actions.UPDATE_ITEM:
            case Actions.CHANGE_CASHBAG_POINT:
                this.setCookie(action.point);
                return update(state, {
                    spent: { $set: !action.point && 0 || action.point },
                });
            case Actions.INITIALIZE:
                return update(state, {
                    spent: { $set: this.getCookie(action.mode) },
                });
            default :
                return state;
        }
    }
}



        // this.props.requestCardno((number) => {
        //     this.setState({
        //         cardnumber: number || '',
        //         registed: !number,
        //     });
        // });

export default new CashbagStore(AppDispatcher);
