import update from 'react-addons-update';
import { ReduceStore } from 'flux/utils';
import AppDispatcher from '../actions/AppDispatcher';
import Actions from '../actions/Actions';
import ItemStore from '../stores/ItemStore';


class AppStore extends ReduceStore {
    getInitialState(){
        return {
            mode: null,
            toggle: true,
            session: null,
            active: false,
            contents: null,
            option: null,
            direct: 0,
            orders: 1,
            noStorageDumItemList: [], //증정상품이 지정되어 있는 상품임에도 불구하고, 증정상품의 재고가 없는 상품의 리스트
            buyCnt: null, //로그인한 사람이 헬로네이쳐에서 구매한 횟수
        }
    }


    /**
     * 쿠키호출
     * @return {[number]}
     */
    getCookie(mode, direct){
        if(mode=='cart' || direct == 1){
            window.cookies.create('cart.order-count', 1, 1);
        }                       
        return Number(window.cookies.read('cart.order-count'));
    }

    reduce(state, action){
        switch(action.type){
            case Actions.RECEIVED_SESSION:
                return update(state, {
                    session: { $set: action.data }
                })
            case Actions.OPEN_DIALOG:
                document.body.style.overflow = 'hidden';
                return update(state, {
                    contents: { $set: action.data },
                    option: { $set: action.option },
                    active: { $set: true },
                });
            case Actions.CLOSE_DIALOG:
                document.body.removeAttribute('style');
                return update(state, {
                    active: { $set: false },
                });
            case Actions.TOGGLE_ITEM:
                return update(state, {
                    toggle: { $set: action.toggle }
                })
            case Actions.CHANGE_ORDER_COUNT:
                return update(state, {
                    orders: { $set: action.count }
                })
            case Actions.UPDATE_ITEM:
            case Actions.DELETE_ITEM:
                 AppDispatcher.waitFor([ItemStore.getDispatchToken()]); 
                 return update(state, {
                    toggle: { $set: ItemStore.checkeds.length > 0 },
                })
            case Actions.RECEIVED_CART_DATA:
                AppDispatcher.waitFor([ItemStore.getDispatchToken()]);  
                return update(state, {
                    toggle: { $set: ItemStore.checkeds.length > 0 },
                    noStorageDumItemList: { $set: action.data.dumList ? action.data.dumList :[] },
                    buyCnt:{$set: action.data.BuyCnt},
                })

            case Actions.INITIALIZE:          
                return update(state, {
                    mode: { $set: action.mode },
                    direct: { $set: action.direct },
                    orders: { $set: this.getCookie(action.mode, action.direct) },
                })
            default:
                return state;
        }
    }

    /**
     * 첫번째 구매 고객인지 확인
     * @return {bollean}
     */
    get isFirstBuyer(){
        return this.getState().buyCnt == 0 ? true :  false;
    }

    /**
     * 로그인 정보반환
     * @return {[object]}
     */
    get session(){
        return this.getState().session;
    }

    /**
     * 사용자 아이디 반환
     * @return {[string]}
     */
    get userId(){
        return this.session && this.session.m_id || null;
    }

    /**
     * 사용자 고유번호 반환
     * @return {[any]}
     */
    get userNo(){
        return this.session && this.session.m_no || null;
    }

    /**
     * 경고창 활성 상태
     * @return {[boolean]}
     */
    get active(){
        return this.getState().active;
    }

    /**
     * 경고창 내용
     * @return {[any]}
     */
    get contents(){
        return this.getState().contents;
    }

    /**
     * 경고창 옵션
     * @return {[object]}
     */
    get option(){
        return this.getState().option;
    }

    /**
     * 상품 전체 선택여부 반환
     * @return {[boolean]}
     */
    get toggle(){
        return this.getState().toggle;
    }

    /**
     * 모듈 실행타입 반환
     * @return {[string]}
     */
    get mode(){
        return this.getState().mode;
    }

    /**
     * 예약배송 회차반환
     * @return {[number]}
     */
    get orders(){
        return this.getState().orders;
    }
    
    /**
     * 곧바로 주문 여부
     * @return {[string]}
     */
    get direct(){
        return this.getState().direct;
    }

    /**
     * 증정상품 리스트 반환
     * @return {[string]}
     */
    get noStorageDumItemList(){
        return this.getState().noStorageDumItemList;
    }


}

export default new AppStore(AppDispatcher);
