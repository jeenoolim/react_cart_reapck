export default {
    //네트워크 통신 에러
    REQUEST_ITEM: {
        code: 4001,
        msg: '상품정보를 불러오지 못했습니다.',
    },
    PARSE_ITEM: {
        code: 8001,
        msg: '잘못된 상품정보가 포함되어 있습니다. 고객선터에 문의 해 주세요.',
    }, 
    REQUEST_SHIPPING_POLICY: {
        code: 4002,
        msg: '배송비관련 정보를 불러오지 못했습니다.',
    },
    REQUEST_COUPON: {
        code: 4003,
        msg: '쿠폰정보를 불러오지 못했습니다. 고객선터에 문의 해 주세요.',
    }, 
    PARSE_COUPON: {
        code: 8003,
        msg: '잘못된 쿠폰 정보가 포함되어 있습니다. 고객선터에 문의 해 주세요.',
    },
    REQUEST_SESSION: {
        code: 4004,
        msg: '사용자 정보를 불러오지 못했습니다. 고객선터에 문의 해 주세요.',
    },
    PARSE_SESSION: {
        code: 8004,
        msg: '잘못된 사용자정보가 포함되어 있습니다. 고객선터에 문의 해 주세요.',
    },
    NETWORK_ERROR: {
        code: 50001,
        msg: '일시적인 사이트 접속 장애가 발생했습니다. 잠시 후 다시 시도해주시기 바랍니다.'
    }

}