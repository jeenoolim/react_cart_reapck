import React from 'react';
import ActionCreators from '../actions/ActionCreators';
import AppStore from '../stores/AppStore';
import ItemStore from '../stores/ItemStore';
import Alert from '../ui/Alert';
import Message from '../dialog/Error';
import CouponStore from '../stores/CouponStore';
import Config from '../Config';

let options,
    requestServer = (url, variables, preload) => {
        return $.ajax({
            url: url,
            type: 'POST',
            cache: false,
            async: true,
            data: variables,
            beforeSend: preload ? loadingBox('show') : false,
        });
    },
    request = {
        /**
         * 세션 정보 불러오기
         * @return {[void]}
         */
        requestSession(){
            options = {};
            options.mode = 'get_session_json';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {  
                res = JSON.parse(res);
                ActionCreators.receivedSession(res);
            })
            .fail(err => {
                ActionCreators.openDialog(<Alert message={ Message.NETWORK_ERROR.msg } />);
            })
        },

        /**
         * 장바구니 아이템 정보 불러오기
         * @return {[void]}
         */
        requestCartData(){
            options = {};
            options.mode = 'get_cart_object';
            options.isDirect = AppStore.direct;
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options, true)
            .done(res => {
                res = JSON.parse(res);
                ActionCreators.receivedCartData(res);
            })
            .fail(err => {
                ActionCreators.openDialog(<Alert message={ Message.NETWORK_ERROR.msg } />);
            })
            .then(() => {
                window.loadingBox('hide');
            })
        },

        /**
         * 쿠폰 정보 불러오기
         * @return {[void]}
         */
        requestCoupons(){
            options = {};
            options.mode = 'php_func_call';
            options.action = 'getCouponList';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {
                res = JSON.parse(res);
                ActionCreators.receivedCoupons(res);
            })
            .fail(err => {
                ActionCreators.openDialog(<Alert message={ Message.NETWORK_ERROR.msg } />);
            })
        },

        /**
         * 배송 정책 불러오기
         * @return {[void]}
         */
        requestShippingPolicy(){
            options = {};
            options.mode = 'php_func_call';
            options.action = 'getDefaultPayPolicy';
            options.arg = 'delivery';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {
                ActionCreators.receivedShippingPolicy(JSON.parse(res));
            })
            .fail(err => {
                ActionCreators.openDialog(<Alert message={ Message.NETWORK_ERROR.msg } />);
            })
        },


        /**
         * 장바구니 전체 아이템 재고요청
         * @return {[void]}
         */
        requestStocks(){
            options = {};
            options.arg = ItemStore.checkedItemsCode.join(',');
            options.mode = 'php_func_call';
            options.action = 'getCurrStock';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {
                let codes = [], values = [];
                JSON.parse(res).map(data => {
                    const { goodsno, runout, buyrunout, usestock, stock } = data;
                    codes = [...codes, goodsno];
                    values = [...values, { runout, buyrunout, usestock, }];
                })
                ActionCreators.receivedStocks(codes, values);
            })
            .fail(err => {
                ActionCreators.requestStocksError(err);
            })
        },


        /**
         * 전체 상품 최대구매 수량에 맞춤요청
         * @param  {[array]}   items
         * @param  {[array]}   counts
         * @param  {Function} callback
         * @return {[type]}
         */
        requestSync(){
            const items = ItemStore.overstocks;
            options = {};
            options.arg = `update_items|${this.requesArguments(items, items.map(item => ({ count: item.stock })))}`;
            options.mode = 'php_func_call';
            options.action = 'simpleCartControl';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {
                ActionCreators.receivedSync(JSON.parse(res));
            })
            .fail(err => {
                ActionCreators.requestSyncError(err);
            })
        },

        /**
         * 장바구니 아이템 수량 변경요청
         * @param  {[array]} items 변경할 아이템 데이터 목록
         * @param  {[number]} count 변경 할 수량
         * @return {[void]}
         * 주문번호, 상품리스트와 상품 갯수를 기호로 연결하여 전송
         * 만약 토글변경관련 요청이면 아이템객체에서 갯수를 가져옴.
         */
        requestItemUpdate(items, values){
            console.log(values)
            options = {};
            options.arg = `update_items|${this.requesArguments(items, values)}`;
            options.mode = 'php_func_call';
            options.action = 'simpleCartControl';
            options.data_type = 'json';
            $.when(requestServer(Config.REQUEST_PHP_FUNCTION, options))
            .done(res => {
                ActionCreators.receivedItemUpdate(JSON.parse(res), items, values);
            })
            .fail(err => {
                ActionCreators.requestItemUpdateError(err);
            })
        },


        /**
         * 장바구니 아이템 삭제요청
         * @param  {[array]} items 삭제 아이템 데이터 목록
         * @return {[void]}
         * 주문번호, 상품리스트와 상품 갯수를 기호로 연결하여 전송
         */
        requestItemDelete(items){
            options = {};
            options.arg = `delete_items|${this.requesArguments(items)}`;
            options.mode = 'php_func_call';
            options.action = 'simpleCartControl';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {
                ActionCreators.receivedItemDelete(res);
            })
            .fail(err => {
                ActionCreators.requestItemDeleteError(err);
            })
        },

        /**
         * 웹사이트 조건과 동일하게 적용하기위해 배송비는 서버쪽 요청
         * @return {[void]}
         * 주문번호, 상품번호, 상품 갯수, 사용한쿠폰의 종류와 가격, 사용한 포인트, 헬로패스대상자여부, 주문페이지로 바로 접근햇는지 여부등을 전달
         */
        requestShippingCost(){
            let items;
            //if(AppStore.mode != 'cart'){
                items = ItemStore.items;
                if(!items.length){
                    return false;
                }

                options = {};
                options.mode = 'php_func_call';
                options.action = 'getDeliveryCall';
                options.data_type = 'json';
                options.arg = 'shpping_cost|';
                options.arg += `${this.requesArguments(items, items.map(item => ({ checked: item.checked ? 'Y' : 'N' })))}|`
                options.arg += `${CouponStore.current && `${CouponStore.current.type},${CouponStore.current.price}` || 0}|`;
                options.arg += `${ItemStore.pointSpent}|`;
                options.arg += `${ItemStore.hellopass}|`;
                options.arg += `${ItemStore.direct}`;
                // console.log(options);
                requestServer(Config.REQUEST_PHP_FUNCTION, options)
                .done(res => {
                    ActionCreators.receivedShippingCost(res);
                })
                .fail(err => {
                    ActionCreators.requestShippingCostError(err);
                })
           // }
        },


        /**
         * 등록된 오케이 캐쉬백 카드번호 요청
         * @return {[void]}
         */
        requestCashbagCardNumber(){
            options = {};
            options.mode = 'php_func_call';
            options.action = 'getOcbCardNo';
            options.data_type = 'json';
            requestServer(Config.REQUEST_PHP_FUNCTION, options)
            .done(res => {
                ActionCreators.receivedCashbagCardNumber(JSON.parse(res));
            })
            .fail(err => {
                ActionCreators.requestCashbagCardNumberError(err);
            })
        },

        /**
         * 오케이 캐쉬백 포인트 요청
         * @param  {[string]} cardnumber
         * @param  {[string]} password
         * @return {[void]}
         * 등록된 유저아이디, 유저번호, 카드번호, 카드비번을 전송
         */
        requestCashbagPoint(cardnumber, password){
            options = {};
            options.mode = 'Auth';
            options.isMobile = 'Y';
            options.Amount = ItemStore.payment;
            options.UserName = AppStore.userId;
            options.SubMallUserId = AppStore.userNo;
            options.OkCardNo = cardnumber;
            options.AuthPwd = password;
            requestServer(Config.REQUEST_CASHBACK, options)
            .done(res => {
                res = JSON.parse(res);
                if(res.ReplyCode === '000000'){
                    ActionCreators.receivedCashbagPoint(res.AvPoint, res.ordno);
                    return false;
                }
                ActionCreators.requestCashbagPointError(res.ReplyMessage, res.ordno);
            })
        },

        /**
         * 아이템 수량변경 요청 옵션
         * @param  {[array]} items
         * @param  {[array]} counts
         * @param  {String} type
         * @return {[void]}
         */

        requesArguments(items, values = null){
            let output = '';
            if(items.length > 0){
                output = `${items[0].id}|${items.map(item => item.code).join(',')}`;
            }
            
            if(Array.isArray(values) && values.length > 0){
                output += `|${values.map(value => Object.values(value)).join(',')}`;
            }
            return output;
        }
    };
export default request;
